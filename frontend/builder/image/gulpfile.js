// Gulp setup.
var gulp = require('gulp');
var p = require('gulp-load-plugins')({
    lazy:    false,
    pattern: ['gulp-*', 'del', 'lazypipe', 'pump', 'run-sequence'],
});

// Util functions
function allFiles(root, ext) {
    return root + '/**/*.' + ext;
}

//p.util.log(p);

// Config
var verbose = false; // trace filenames
var isDevel = !!process.env.HUNI_DEVELOPMENT; // sorry about the !!

var srcRoot  = process.env.SRC_DIR;
var destRoot = process.env.DEST_DIR;
var tmpRoot  = process.env.TEMP_DIR;
var angularDir  = 'angular-1.4.14/';

var srcHtml = [ allFiles(srcRoot + '/src/html',       'html') ];
var srcJs   = [ allFiles(srcRoot + '/src/js',         'js'),
                         srcRoot + '/src/config/docker.js', ];
var widgetCfg = srcRoot + '/src/config/wgt-docker.js';
var widgetJs =       [ widgetCfg, srcRoot + '/src/widgets/huni-widget.js' ];
// Only the top-level huni-widget.js should be bundled with the config.
// Any other huni-widget-*.js should _not_ be bundled.
var widgetSearchJs = [ srcRoot + '/src/widgets/huni-widget-search.js' ];

var libJs = [
	"jquery-1.11.3.js",
	"bootstrap-3.3.5/js/bootstrap.js",
	angularDir + "angular.js",
	angularDir + "angular-animate.js",
	angularDir + "angular-cookies.js",
	angularDir + "angular-route.js",
	"angulartics-0.15.19/angulartics.js",
	"angulartics-0.15.19/angulartics-ga.js",
	"angular-shims-placeholder-0.2.0/angular-shims-placeholder.js",
	"angular-summernote-0.4.0/src/angular-summernote.js",
	"d3-3.5.6/d3.js",
	"moment-2.10.6/moment.js",
	"summernote-0.6.16/summernote.js",
	"summernote-0.6.16/plugin/summernote-ext-video.js",
	"ui-bootstrap-tpls-0.13.3.js",
	"underscore-1.8.3/underscore.js",
	"spin.min.js",
].map(path => srcRoot + '/vendor-lib/' + path);

gulp.task('src', function() {
    // Build the js source files
    var out = 'huni-src.js';
    p.del.sync([destRoot + '/' + out]);
    return gulp.src(srcJs.concat([ tmpRoot + '/partials.js' ]))
        .pipe(p.order([ "**/app.js" ]))
        .pipe(javascript(out, destRoot))
        ;
});

gulp.task('lib', function() {
    // Build the js vendor-lib files
    var out = 'huni-lib.js';
    p.del.sync([destRoot + '/' + out]);
    return gulp.src(libJs)
        .pipe(javascript(out, destRoot, { noBabel: true }))
        ;
});

gulp.task('widget', function() {
    return gulp.src(widgetJs)
        .pipe(javascript('huni-widget.js', destRoot))
        ;
});

gulp.task('widget-search', function() {
    return gulp.src(widgetSearchJs)
        .pipe(javascript('huni-widget-search.js', destRoot))
        ;
});

function javascript(name, dir, opt = { }) {
    // lazypipe lets us compose pipe stages into a single stage
    // Note the semantics are different, we don't call the functions
    // eg. .pipe(p.sourcemaps.init()) becomes .pipe(p.sourcemaps.init)
    // and parameters get listed after the function
    // eg. .pipe(p.concat(out)) becomes .pipe(p.concat, out)
    function IF(cond, pipeFunc) {
        return cond ? pipeFunc : p.util.noop;
    }

    return p.lazypipe()
        .pipe(IF(verbose, p.debug), { title: name + ':' })
        .pipe(p.sourcemaps.init)
        .pipe(IF(!opt.noBabel, p.babel), { presets: ['es2015'] })
        .pipe(IF(!opt.noConcat, p.concat), name)
        .pipe(IF(!isDevel, p.uglify))
        .pipe(p.sourcemaps.write, '.')
        .pipe(gulp.dest, destRoot)
        (); // final call is the builder
}

gulp.task('partials', function() {
    // Inline the html partial templates into partials.js
    var templateCacheOpts = {
        module: 'huni',
    };

    return gulp.src(srcHtml)
        //.pipe(p.debug({title: 'html:'}))
        .pipe(p.minifyHtml())
        .pipe(p.angularTemplatecache('partials.js', templateCacheOpts))
        .pipe(gulp.dest(tmpRoot))
        ;
});

gulp.task('partials-src', function(callback) {
    p.runSequence('partials', 'src', callback);
});

gulp.task('watch', function() {
    function watch(sources, tasks) {
        var watchOptions = { interval: 1000 };
        gulp.watch(sources, watchOptions, tasks);
    }

    // Watch for changes and trigger rebuilds as necessary
    watch(srcJs,      ['src']);
    watch(libJs,      ['lib']);
    watch(widgetJs,   ['widget']);
    watch(widgetSearchJs, ['widget-search']);
    watch(srcHtml,    ['partials-src']);
});

gulp.task('build', ['partials-src', 'lib', 'widget', 'widget-search']);
