# HuNI search and browse...


## Repository Layout
 - app: the HuNI AngularJS app
 - scripts: a script to start a python http server for development

## Updating the JS lib file
To simplify JS file inclusions there is a script publish.sh in the js folder
which will compile all the relevant JS files into a single source file called
lib.js.

It then rsync's the app to some location on the system (where a webserver
knows to serve the content) from where it is served. Think of this as a 
compile / deploy tool for development.

You don't have to use it, though if you do, ensure the rsync command in the
script is correct for the setup you're using.


