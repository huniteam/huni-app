(function(self) {

  // search widget html
  var wgtHtml = '\<span class="logo"\>\<\/span\>\<span class="container"\>\<span class="message"\>Search HuNI\'s \<span class="' + self.wgtname + '-search-count"\>\<\/span\> records\<\/span\>\<form onsubmit="return ' + self.wgtname + '.search_lab(this);"\>\<input type="text" name="q"\>\<nbsp;\>\<input type="submit" value="go"\>\<\/form\>\<\/span\>';

  self.insertWgts('search', wgtHtml);

  // helper function to work around issue doing a form get with params to an
  //  AngularJS app (the query and hash components get reversed via form get)
  self.search_lab = function(wgtForm) {
    location.href = self.lab + '/#/results?q=' + wgtForm.elements["q"].value;
    return false;
  };

  // callback to update record count in widget html
  var updateCount = function(jsonpResp) {
    var allElems = document.getElementsByTagName('*');
    for (var i = 0, n = allElems.length; i < n; i++) {
      if ((' ' + allElems[i].className + ' ').indexOf(' ' + self.wgtname + '-search-count ') > -1) {
        allElems[i].innerHTML = jsonpResp.response.numFound;
      }
    }
  };

  // call to Solr service to return count of all rows
  //  (don't need results so rows=0)
  self.solrRequest('q=*&rows=0', updateCount);

  return self;

})(huni_097b8a38_9eb1_4bdb_9c53_bfffa9a924c4);
