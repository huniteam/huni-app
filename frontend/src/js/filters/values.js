angular.module('huni').filter('values', function() {
    // Take a hash, and return an array of the values.
    return function(input) {
        return _.values(input);
    };
});
