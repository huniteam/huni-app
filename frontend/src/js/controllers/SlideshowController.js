angular.module('huni').controller('SlideshowController', [ '$scope',
function ($scope) {
    $scope.slideInterval = 4000;
    $scope.slides = [
        { image: "images/huni-banner-3.jpg", active: false },
        { image: "images/huni-banner-1.jpg", active: false },
        { image: "images/huni-banner-2.jpg", active: false }
    ];
}]);
