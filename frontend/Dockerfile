FROM node:10 AS static-builder

ENV BUILD_ROOT=/build \
    SRC_DIR=in \
    DEST_DIR=out \
    TEMP_DIR=tmp

WORKDIR ${BUILD_ROOT}

# Install node packages
RUN npm install -s gulp-cli -g

# Copy this file separately to keep the npm stuff cached
COPY builder/image/package.json ${BUILD_ROOT}
RUN npm install -s

# Now copy all the rest.
COPY builder/image/ ${BUILD_ROOT}
COPY src/ ${SRC_DIR}/src
COPY vendor-lib/ ${SRC_DIR}/vendor-lib
COPY app/ ${DEST_DIR}

RUN mkdir -p ${TEMP_DIR}

RUN gulp build

RUN for ext in css html js json map svg xml; do \
        find ${DEST_DIR} -type f -iname \*.$ext \
            -exec gzip -v -9 -k {} \; ; \
    done

ARG GIT_SHA=0
ENV GIT_SHA=$GIT_SHA

RUN echo $GIT_SHA > ${DEST_DIR}/version \
 && echo "{ \"git\":\"$GIT_SHA\" }" > ${DEST_DIR}/version

CMD ['/build/start.sh']

#------------------------------------------------------------------------------

FROM nginx:mainline

COPY nginx.conf /etc/nginx

# Copy the pre-cooked app directory into the image
COPY --from=static-builder /build/out /usr/share/nginx/html

ARG GIT_SHA=0
RUN echo "{\"git\":\"$GIT_SHA\"}" > /usr/share/nginx/html/version.json
ENV GIT_SHA=$GIT_SHA

# vim:syntax=dockerfile
