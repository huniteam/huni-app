#!/bin/bash -e

usage() {
    echo usage: $0 'frontend|backend|all' 1>&2
    exit 1;
}

export HUNI_DEVELOPMENT=''

if [[ $# -ne 1 ]]; then
    usage
fi

export COMPOSE_FILE="compose.yaml:compose.build.yaml"
GIT_SHA=$( git rev-parse --verify HEAD )

if [[ $1 == 'frontend' || $1 == 'all' ]]; then
    docker-compose build --pull --build-arg GIT_SHA=$GIT_SHA frontend
fi

if [[ $1 == 'backend' || $1 == 'all' ]]; then
    docker-compose build --pull --build-arg GIT_SHA=$GIT_SHA backend
fi
