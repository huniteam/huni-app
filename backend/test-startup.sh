#!/bin/bash -e

export HUNI_TEST_DB_PREFIX=huni_test_$( hostname )_

eval $( /fixture/postgres/wait.sh )
eval $( /fixture/neo4j/wait.sh )

export HUNI_TEST_TEMPLATE_DB=huni-db-test-template

export PGPASSWORD="$POSTGRES_PASSWORD"
export PGUSER="$POSTGRES_USER"

if ! psql -l | grep -q " $HUNI_TEST_TEMPLATE_DB "; then
    createdb $HUNI_TEST_TEMPLATE_DB
fi
SQITCH_TARGET=testing

export SQITCH_CONFIG=/tmp/sqitch-devel.conf
cat sqitch.conf > $SQITCH_CONFIG
cat <<END_CONFIG >> $SQITCH_CONFIG
[target "$SQITCH_TARGET"]
	uri = db:pg://${POSTGRES_USER}:${POSTGRES_PASSWORD}@${PGHOST}/${HUNI_TEST_TEMPLATE_DB}
[engine "pg"]
	target = $SQITCH_TARGET
END_CONFIG

export HUNI_TEST_PG="$( sqitch dsn )"

sqitch deploy

set +e
prove -I t/lib "$@"

psql -l | grep " $HUNI_TEST_DB_PREFIX" | awk '{ print $1 }' | xargs -n 1 dropdb
psql -l
