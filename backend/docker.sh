#!/bin/bash

IMAGE_NAME=huniteam/huni-backend

if [ -n $2 ]; then
    TAG_NAME=$2
    IMAGE_NAME=$IMAGE_NAME:$TAG_NAME
fi

PG_IMAGE=postgres:9.3.5
PG_CONTAINER=huni-postgres-prove
PG_PASSWORD=foo

case $1 in

    build)
        docker build -t $IMAGE_NAME .
        if [[ -n $TAG_NAME ]]; then
            git tag -f docker-image/$TAG_NAME
        fi
        ;;

    carton)
        docker run --rm -w /pwd -v $PWD:/pwd $IMAGE_NAME carton install
        ;;

    push)
        echo Pushing $IMAGE_NAME
        docker push $IMAGE_NAME
        ;;

    prove)
        docker rm -f $PG_CONTAINER
        docker run -d --name $PG_CONTAINER \
                   -e POSTGRES_PASSWORD=$PG_PASSWORD \
                   $PG_IMAGE
        docker run --rm --link $PG_CONTAINER:pg \
                   -e PGPASSWORD=$PG_PASSWORD \
                   -v $PWD/t:/opt/backend/t \
                   $IMAGE_NAME \
                   t/docker-tests.sh
        docker rm -f $PG_CONTAINER
        ;;

    *)
        echo "usage: $0 [build|start|stop]"
        exit 1

        ;;

esac

