FROM perl:5.36

# Copy the cpanfiles first, so we can avoid reinstalling libs unless we really
# need to.
WORKDIR /opt/backend
COPY cpanfile cpanfile.snapshot /opt/backend/

RUN apt-get update \
 && DEBIAN_FRONTEND=noninteractive apt-get install -y \
        netcat-traditional \
        postgresql-client \
 && cpanm -n -q App::cpm Carton::Snapshot \
 && cpm install --global --color --verbose \
 && rm -rf /root/.cpanm /var/lib/apt/lists

# Now copy our own files
COPY lib/ /opt/backend/lib/
COPY environments/docker.yml /opt/backend/config.yml
COPY app.psgi docker-cmd.sh docker-entrypoint.sh neo4j-schema.yaml \
        sqitch.conf /opt/backend/
COPY sqitch.d/ /opt/backend/sqitch.d/
COPY tools/ /opt/backend/tools/
COPY cms/ /opt/backend/cms/

ENTRYPOINT ["/opt/backend/docker-entrypoint.sh"]
CMD ["carton", "exec", "/opt/backend/docker-cmd.sh"]
EXPOSE 80

ARG GIT_SHA=0
ENV GIT_SHA=$GIT_SHA
