-- Verify linktype-table

BEGIN;

SELECT
    id,
    name,
    user_id,
    from_id,
    to_id,
    pair_id,
    created_utc,
    modified_utc
  FROM huni.linktype
  WHERE FALSE;

SELECT has_function_privilege(
    'huni.create_linktype(TEXT, TEXT, TEXT, INTEGER)', 'execute');
SELECT has_function_privilege(
    'huni.create_linktype(TEXT, TEXT, TEXT, TEXT, INTEGER)', 'execute');

ROLLBACK;
