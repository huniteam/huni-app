-- Verify collection-in-solr-flag

BEGIN;

SELECT
    in_solr
  FROM huni.collection
  WHERE FALSE;

ROLLBACK;
