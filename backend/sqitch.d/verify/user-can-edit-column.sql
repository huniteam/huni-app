-- Verify huni-backend:user-can-edit-column on pg

BEGIN;

SELECT can_edit FROM huni.user WHERE false;

ROLLBACK;
