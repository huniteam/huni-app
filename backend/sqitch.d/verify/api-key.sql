-- Verify api-key

BEGIN;

SELECT
    api_username,
    api_hashed_key,
    api_hash_method
  FROM huni.user
  WHERE FALSE;

ROLLBACK;
