-- Verify collection-table-timestamp

BEGIN;

-- This will fail if the trigger wasn't installed.
-- We don't really want to do destructive changes here, but its the best I've
-- got atm.
DROP TRIGGER update_timestamp ON huni.collection;

ROLLBACK;
