-- Verify collection-timestamp-function

BEGIN;

SELECT has_function_privilege('huni.update_collection_timestamp()', 'execute');

ROLLBACK;
