-- Verify collection-table

BEGIN;

SELECT
    id,
    user_id,
    name,
    synopsis,
    is_public,
    created_utc,
    modified_utc
  FROM huni.collection
  WHERE FALSE;

ROLLBACK;
