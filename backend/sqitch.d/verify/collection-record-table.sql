-- Verify collection-record-table

BEGIN;

SELECT collection_id, docid, created_utc
  FROM huni.collection_record
  WHERE FALSE;

ROLLBACK;
