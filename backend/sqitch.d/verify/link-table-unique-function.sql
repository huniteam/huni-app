-- Verify link-table-unique-function

BEGIN;

-- This will fail if the trigger wasn't installed.
-- We don't really want to do destructive changes here, but its the best I've
-- got atm.

DROP TRIGGER check_linkpair_unique ON huni.link;

DROP FUNCTION huni.check_linkpair_unique();

ROLLBACK;
