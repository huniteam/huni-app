-- Verify aaf-jti-table

BEGIN;

SELECT
    jti,
    expiry
  FROM HUNI.aaf_jti
  WHERE FALSE;

ROLLBACK;
