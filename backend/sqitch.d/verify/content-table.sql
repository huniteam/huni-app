-- Verify huni-backend:content-table on pg

BEGIN;

SELECT
    id,
    version,
    content,
    user_id,
    created_utc
  FROM
    huni.content
  WHERE false;

ROLLBACK;
