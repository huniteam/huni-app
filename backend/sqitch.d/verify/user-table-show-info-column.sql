-- Verify huni-backend:user-table-show-info-column on pg

BEGIN;

SELECT show_profile_info
  FROM huni.user
 WHERE false;

ROLLBACK;
