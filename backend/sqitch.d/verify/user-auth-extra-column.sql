-- Verify huni-backend:user-auth-extra-column on pg

BEGIN;

SELECT auth_extra
  FROM huni.user
 WHERE false;

ROLLBACK;
