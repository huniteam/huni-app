-- Revert huni-backend:user-table-show-info-column from pg

BEGIN;

ALTER TABLE huni.user
  DROP COLUMN IF EXISTS show_profile_info;

COMMIT;
