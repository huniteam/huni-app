-- Revert collection-record-table

BEGIN;

DROP TABLE IF EXISTS huni.collection_record;

COMMIT;
