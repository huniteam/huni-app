-- Revert collection-table-timestamp

BEGIN;

DROP TRIGGER IF EXISTS update_timestamp ON huni.collection;

COMMIT;
