-- Revert huni-backend:collection-user-table from pg

BEGIN;

DROP TABLE IF EXISTS huni.collection_user;

COMMIT;
