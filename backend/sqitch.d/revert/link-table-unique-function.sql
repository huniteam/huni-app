-- Revert link-table-unique-function

BEGIN;

DROP TRIGGER IF EXISTS check_linkpair_unique ON huni.link;

DROP FUNCTION IF EXISTS huni.check_linkpair_unique();

COMMIT;
