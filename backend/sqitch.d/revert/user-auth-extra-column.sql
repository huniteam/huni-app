-- Revert huni-backend:user-auth-extra-column from pg

BEGIN;

ALTER TABLE huni.user
  DROP COLUMN auth_extra;

COMMIT;
