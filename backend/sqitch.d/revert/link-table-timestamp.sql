-- Revert link-table-timestamp

BEGIN;

DROP TRIGGER IF EXISTS update_timestamp ON huni.link;

COMMIT;
