-- Revert linktype-table-timestamp

BEGIN;

DROP TRIGGER IF EXISTS update_timestamp ON huni.linktype;

COMMIT;
