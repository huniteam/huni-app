-- Revert huni-backend:collection-role-table from pg

BEGIN;

DROP TABLE IF EXISTS huni.collection_role;

COMMIT;
