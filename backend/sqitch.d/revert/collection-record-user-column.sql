-- Revert huni-backend:collection-record-user-column from pg

BEGIN;

ALTER TABLE huni.collection_record
  DROP COLUMN IF EXISTS user_id;

COMMIT;
