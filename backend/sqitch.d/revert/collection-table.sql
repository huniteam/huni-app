-- Revert collection-table

BEGIN;

DROP TABLE IF EXISTS huni.collection;

COMMIT;
