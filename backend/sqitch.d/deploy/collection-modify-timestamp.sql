-- Deploy collection-modify-timestamp
-- requires: collection-timestamp-function

BEGIN;

CREATE TRIGGER update_timestamp AFTER INSERT OR DELETE
    ON huni.collection_record FOR EACH ROW EXECUTE PROCEDURE
    huni.update_collection_timestamp();

COMMIT;
