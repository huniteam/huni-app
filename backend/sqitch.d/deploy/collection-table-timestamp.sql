-- Deploy collection-table-timestamp
-- requires: collection-table
-- requires: update-timestamp-function

BEGIN;

CREATE TRIGGER update_timestamp BEFORE UPDATE
    ON huni.collection FOR EACH ROW EXECUTE PROCEDURE
    huni.update_timestamp();

COMMIT;
