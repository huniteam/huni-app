-- Deploy linktype-table
-- requires: category-table

BEGIN;

CREATE TABLE huni.linktype (
    id      SERIAL PRIMARY KEY,

    name    text NOT NULL
            CONSTRAINT nonempty_name CHECK (length(btrim(name)) > 0),

    user_id INTEGER NOT NULL REFERENCES huni.user(id)
            ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE,

    from_id INTEGER NOT NULL REFERENCES huni.category(id)
            ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE,
    to_id   INTEGER NOT NULL REFERENCES huni.category(id)
            ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE,

    pair_id INTEGER NOT NULL REFERENCES huni.linktype(id),

    created_utc  timestamp NOT NULL DEFAULT (now() at time zone 'utc'),
    modified_utc timestamp NOT NULL DEFAULT (now() at time zone 'utc')
);

-- Create a pair of (possibly the same) links between two
-- (possibly the same) categories
CREATE FUNCTION huni.create_linktype(from_name TEXT,
                                     to_name TEXT,
                                     linktype_name TEXT,
                                     converse_name TEXT,
                                     user_id INTEGER
                                        DEFAULT huni.system_user_id())
    RETURNS VOID AS $$
DECLARE
    first_id    INTEGER;
    second_id   INTEGER;
    new_from_id INTEGER;
    new_to_id   INTEGER;
    duplicates  INTEGER;
BEGIN
    -- First of all, look up the category ids from the names.
    SELECT id INTO new_from_id FROM huni.category WHERE name = from_name;
    SELECT id INTO new_to_id   FROM huni.category WHERE name = to_name;

    -- Now check that we don't have this linktype already. This doesn't
    -- work as a unique constraint as we are now allowing these:
    --  parent/child
    --  parent/son
    --  parent/daughter
    --  mother/child
    --  mother/son
    --  mother/daughter
    --  father/child
    --  father/son
    --  father/daughter
    SELECT * INTO duplicates
        FROM huni.linktype lhs
        JOIN huni.linktype rhs
            ON lhs.pair_id = rhs.id
        WHERE lhs.from_id = new_from_id
          AND lhs.to_id   = new_to_id
          AND lhs.name    = linktype_name
          AND rhs.name    = converse_name;

    IF FOUND THEN
        RAISE EXCEPTION 'linktype "%/% <=> %/%" already exists',
            from_name, linktype_name, to_name, converse_name;
    END IF;

    INSERT INTO huni.linktype (name, user_id, from_id, to_id, pair_id)
        VALUES (
            -- set this to point to itself for now
            linktype_name, user_id, new_from_id, new_to_id, lastval()
        ) RETURNING id INTO first_id;

    IF from_name != to_name OR linktype_name != converse_name THEN
        -- If we need a second row, add it in ...
        INSERT INTO huni.linktype (name, user_id, from_id, to_id, pair_id)
            VALUES (
                converse_name, user_id, new_to_id, new_from_id, first_id
            ) RETURNING id INTO second_id;

        -- ... and then fix up the pair_id from the first row
        UPDATE huni.linktype
            SET pair_id = second_id -- now fix it back up
            WHERE id = first_id;
    END IF;
END
$$ LANGUAGE 'plpgsql';

-- Create a same-named link between two categories
-- This is just a shortcut for the 4-arg version
CREATE FUNCTION huni.create_linktype(from_name TEXT,
                                     to_name TEXT,
                                     linktype_name TEXT,
                                     user_id INTEGER
                                        DEFAULT huni.system_user_id())
    RETURNS VOID AS
$$
BEGIN
    PERFORM huni.create_linktype(from_name, to_name,
                                 linktype_name, linktype_name, user_id);
END
$$ LANGUAGE 'plpgsql';


-- Temporarily redirect output to /dev/null to keep sqitch output nice
\o /dev/null

-- Insert some data

SELECT huni.create_linktype('Concept',      'Concept',      'Same As');
SELECT huni.create_linktype('Concept',      'Concept',      'Associated With');
SELECT huni.create_linktype('Concept',      'Concept',      'Broader Than',         'Narrower Than');

SELECT huni.create_linktype('Concept',      'Event',        'Associated With');
SELECT huni.create_linktype('Concept',      'Event',        'Genre Of',             'Has Genre');
SELECT huni.create_linktype('Concept',      'Event',        'Expressed In',         'Expression Of');
SELECT huni.create_linktype('Concept',      'Event',        'Theme Of',             'Has Theme');
SELECT huni.create_linktype('Concept',      'Event',        'Subject Of',           'Has Subject');
SELECT huni.create_linktype('Concept',      'Event',        'Introduced At',        'Introduced');

SELECT huni.create_linktype('Concept',      'Organisation', 'Associated With');
SELECT huni.create_linktype('Concept',      'Organisation', 'Originated By',        'Originator Of');
SELECT huni.create_linktype('Concept',      'Organisation', 'Guiding Principle Of', 'Has Guiding Principle');
SELECT huni.create_linktype('Concept',      'Organisation', 'Goal Of',              'Has Goal');
SELECT huni.create_linktype('Concept',      'Organisation', 'Investigated By',      'Investigator Of');
SELECT huni.create_linktype('Concept',      'Organisation', 'Was Conceived By',     'Conceived');
SELECT huni.create_linktype('Concept',      'Organisation', 'Promoted By',          'Promoted');
SELECT huni.create_linktype('Concept',      'Organisation', 'Opposed By',           'Opposed');

SELECT huni.create_linktype('Concept',      'Person',       'Associated With',      'Associated With');
SELECT huni.create_linktype('Concept',      'Person',       'Originated By',        'Originator Of');
SELECT huni.create_linktype('Concept',      'Person',       'Guiding Principle Of', 'Has Guiding Principle');
SELECT huni.create_linktype('Concept',      'Person',       'Goal Of',              'Has Goal');
SELECT huni.create_linktype('Concept',      'Person',       'Investigated By',      'Investigator Of');
SELECT huni.create_linktype('Concept',      'Person',       'Was Conceived By',     'Conceived');
SELECT huni.create_linktype('Concept',      'Person',       'Espoused By',          'Promoted');
SELECT huni.create_linktype('Concept',      'Person',       'Opposed By',           'Opposed');

SELECT huni.create_linktype('Concept',      'Place',        'Associated With',      'Associated With');
SELECT huni.create_linktype('Concept',      'Place',        'Originated In',        'Originating Place Of');

SELECT huni.create_linktype('Concept',      'Work',         'Associated With');
SELECT huni.create_linktype('Concept',      'Work',         'Genre Of',             'Has Genre');
SELECT huni.create_linktype('Concept',      'Work',         'Expressed In',         'Expression Of');
SELECT huni.create_linktype('Concept',      'Work',         'Theme Of',             'Has Theme');
SELECT huni.create_linktype('Concept',      'Work',         'Subject Of',           'Has Subject');
SELECT huni.create_linktype('Concept',      'Work',         'Referenced In',        'References');

SELECT huni.create_linktype('Event',        'Event',        'Same As');
SELECT huni.create_linktype('Event',        'Event',        'Associated With');
SELECT huni.create_linktype('Event',        'Event',        'Preceded By',          'Followed By');

SELECT huni.create_linktype('Event',        'Organisation', 'Associated With',      'Associated With');
SELECT huni.create_linktype('Event',        'Organisation', 'Held By',              'Held');
SELECT huni.create_linktype('Event',        'Organisation', 'Involved',             'Involved In');

SELECT huni.create_linktype('Event',        'Person',       'Associated With');
SELECT huni.create_linktype('Event',        'Person',       'Organised by',         'Organiser Of');
SELECT huni.create_linktype('Event',        'Person',       'Involved',             'Involved In');
SELECT huni.create_linktype('Event',        'Person',       'Influenced',           'Influenced By');
SELECT huni.create_linktype('Event',        'Person',       'Honoured',             'Was Honoured By');

SELECT huni.create_linktype('Event',        'Place',        'Associated With');
SELECT huni.create_linktype('Event',        'Place',        'Located In',           'Location Of');

SELECT huni.create_linktype('Event',        'Work',         'Associated With');
SELECT huni.create_linktype('Event',        'Work',         'Presented',            'Presented At');
SELECT huni.create_linktype('Event',        'Work',         'Involved',             'Involved In');
SELECT huni.create_linktype('Event',        'Work',         'Exhibited',            'Exhibited At');

SELECT huni.create_linktype('Organisation', 'Organisation', 'Same As');
SELECT huni.create_linktype('Organisation', 'Organisation', 'Associated With');
SELECT huni.create_linktype('Organisation', 'Organisation', 'Sub Organisation Of',  'Super Organisation Of');

SELECT huni.create_linktype('Organisation', 'Person',       'Associated With');
SELECT huni.create_linktype('Organisation', 'Person',       'Has Role Played By',   'Has Role In');
SELECT huni.create_linktype('Organisation', 'Person',       'Initated By',          'Initiated');
SELECT huni.create_linktype('Organisation', 'Person',       'Led By',               'Leader Of');
SELECT huni.create_linktype('Organisation', 'Person',       'Honoured',             'Was Honoured By');

SELECT huni.create_linktype('Organisation', 'Place',        'Associated With');
SELECT huni.create_linktype('Organisation', 'Place',        'Located In',           'Location Of');
SELECT huni.create_linktype('Organisation', 'Place',        'Founded In',           'Founding Place Of');

SELECT huni.create_linktype('Organisation', 'Work',         'Associated With');
SELECT huni.create_linktype('Organisation', 'Work',         'Publisher Of',         'Published By');
SELECT huni.create_linktype('Organisation', 'Work',         'Producer Of',          'Produced By');
SELECT huni.create_linktype('Organisation', 'Work',         'Owns Rights To',       'Rights Owned By');

SELECT huni.create_linktype('Person',       'Person',       'Same As');
SELECT huni.create_linktype('Person',       'Person',       'Associated With');
SELECT huni.create_linktype('Person',       'Person',       'Spouse Of');
SELECT huni.create_linktype('Person',       'Person',       'Parent Of',            'Child Of');
SELECT huni.create_linktype('Person',       'Person',       'Sibling Of');
SELECT huni.create_linktype('Person',       'Person',       'Relative Of');
SELECT huni.create_linktype('Person',       'Person',       'Employer Of',          'Employee Of');
SELECT huni.create_linktype('Person',       'Person',       'Collaborator Of');
SELECT huni.create_linktype('Person',       'Person',       'Friend Of');
SELECT huni.create_linktype('Person',       'Person',       'Enemy Of');
SELECT huni.create_linktype('Person',       'Person',       'Influenced By',        'Influenced');
SELECT huni.create_linktype('Person',       'Person',       'Teacher Of',           'Student Of');
SELECT huni.create_linktype('Person',       'Person',       'Admirer Of',           'Admired');
SELECT huni.create_linktype('Person',       'Person',       'Follower Of',          'Followed');
SELECT huni.create_linktype('Person',       'Person',       'Romantically Involved With');

SELECT huni.create_linktype('Person',       'Place',        'Associated With');
SELECT huni.create_linktype('Person',       'Place',        'Born In',              'Birth Place Of');
SELECT huni.create_linktype('Person',       'Place',        'Died In',              'Death Place Of');
SELECT huni.create_linktype('Person',       'Place',        'Lived In',             'Living Place Of');
SELECT huni.create_linktype('Person',       'Place',        'Founded',              'Was Founded By');
SELECT huni.create_linktype('Person',       'Place',        'Owner Of',             'Owned By');
SELECT huni.create_linktype('Person',       'Place',        'Located In',           'Location Of');
SELECT huni.create_linktype('Person',       'Place',        'Influenced By',        'Influenced');

SELECT huni.create_linktype('Person',       'Work',         'Associated With');
SELECT huni.create_linktype('Person',       'Work',         'Creator Of',           'Created By');
SELECT huni.create_linktype('Person',       'Work',         'Influenced By',        'Influenced');
SELECT huni.create_linktype('Person',       'Work',         'Director Of',          'Directed By');
SELECT huni.create_linktype('Person',       'Work',         'Subject Of',           'Has Subject');
SELECT huni.create_linktype('Person',       'Work',         'Participated In',      'Had Participant');

SELECT huni.create_linktype('Place',        'Place',        'Same As');
SELECT huni.create_linktype('Place',        'Place',        'Associated With');
SELECT huni.create_linktype('Place',        'Place',        'Has Part',             'Part Of');

SELECT huni.create_linktype('Place',        'Work',         'Associated With');
SELECT huni.create_linktype('Place',        'Work',         'Located In',           'Location Of');
SELECT huni.create_linktype('Place',        'Work',         'Setting Of',           'Set In');
SELECT huni.create_linktype('Place',        'Work',         'Creation Place Of',    'Created In');

SELECT huni.create_linktype('Work',         'Work',         'Same As');
SELECT huni.create_linktype('Work',         'Work',         'Associated With');
SELECT huni.create_linktype('Work',         'Work',         'References',           'Is Referenced By');
SELECT huni.create_linktype('Work',         'Work',         'Influenced By',        'Influenced');

-- Restore regular output
\o

COMMIT;
