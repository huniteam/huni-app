-- Deploy huni-backend:collection-role-table to pg
-- requires: schema

BEGIN;

SET client_min_messages = 'warning';

CREATE TABLE huni.collection_role (
    id TEXT PRIMARY KEY
);

INSERT INTO huni.collection_role (id) VALUES
    ('owner'),
    ('read-write'),
    ('read-only');

COMMIT;
