-- Deploy user-table
-- requires: schema

BEGIN;

SET client_min_messages = 'warning';

CREATE TABLE huni.user (
    id              SERIAL PRIMARY KEY,

    name            text NOT NULL
                    CONSTRAINT nonempty_name CHECK (length(btrim(name)) > 0),
    email           text,
    institutions    text,
    description     text,
    interests       text,
    collaboration_interests     text,
    auth_userid     text NOT NULL,
    auth_domain     text NOT NULL,
    created_utc     timestamp NOT NULL DEFAULT (now() at time zone 'utc'),
    modified_utc    timestamp NOT NULL DEFAULT (now() at time zone 'utc'),

    CONSTRAINT auth_userId_domain_unique UNIQUE(auth_userid, auth_domain)
);

INSERT INTO huni.user (name, auth_userid, auth_domain)
    VALUES ('HuNI System', 'huni', 'huni');

CREATE FUNCTION huni.system_user_id()
    RETURNS int IMMUTABLE LANGUAGE SQL AS $$
    SELECT id
        FROM huni.user
        WHERE auth_userid = 'huni' AND auth_domain = 'huni';
$$;

COMMIT;
