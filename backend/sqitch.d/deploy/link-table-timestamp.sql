-- Deploy link-table-timestamp
-- requires: link-table
-- requires: update-timestamp-function

BEGIN;

CREATE TRIGGER update_timestamp BEFORE UPDATE
    ON huni.link FOR EACH ROW EXECUTE PROCEDURE
    huni.update_timestamp();

COMMIT;
