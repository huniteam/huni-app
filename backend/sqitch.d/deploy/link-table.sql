-- Deploy link-table
-- requires: linktype-table
-- requires: user-table

BEGIN;

SET client_min_messages = 'warning';

CREATE TABLE huni.link (
    id              SERIAL PRIMARY KEY,

    user_id         INTEGER NOT NULL REFERENCES huni.user(id)
                    ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE,

    synopsis        text,

    from_docid      text NOT NULL,
    to_docid        text NOT NULL,

    linktype_id     INTEGER NOT NULL REFERENCES huni.linktype(id)
                    ON UPDATE CASCADE ON DELETE CASCADE,

    created_utc     timestamp NOT NULL DEFAULT (now() at time zone 'utc'),
    modified_utc    timestamp NOT NULL DEFAULT (now() at time zone 'utc'),

    CONSTRAINT no_self_links CHECK(from_docid != to_docid)
);


COMMIT;
