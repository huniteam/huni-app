-- Deploy huni-backend:user-can-edit-column to pg
-- requires: user-table

BEGIN;

ALTER TABLE huni.user ADD COLUMN can_edit boolean NOT NULL DEFAULT false;

COMMIT;
