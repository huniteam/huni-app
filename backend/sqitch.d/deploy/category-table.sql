-- Deploy category-table
-- requires: schema

BEGIN;

SET client_min_messages = 'warning';

CREATE TABLE huni.category (
    id      SERIAL PRIMARY KEY,

    name    text NOT NULL,

    CONSTRAINT unique_name UNIQUE(name)
);

INSERT INTO huni.category (name) VALUES
    ('Concept'),
    ('Event'),
    ('Organisation'),
    ('Person'),
    ('Place'),
    ('Work');

COMMIT;
