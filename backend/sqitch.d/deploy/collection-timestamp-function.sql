-- Deploy collection-timestamp-function
-- requires: collection-record-table

BEGIN;

CREATE OR REPLACE FUNCTION huni.update_collection_timestamp()
RETURNS TRIGGER AS $$
BEGIN
    IF (TG_OP = 'DELETE') THEN
        UPDATE huni.collection
            SET modified_utc = now() at time zone 'utc'
            WHERE id = OLD.collection_id;
    ELSIF (TG_OP = 'INSERT') THEN
        UPDATE huni.collection
            SET modified_utc = now() at time zone 'utc'
            WHERE id = NEW.collection_id;
    END IF;
    RETURN NULL;
END;
$$ language 'plpgsql';

COMMIT;
