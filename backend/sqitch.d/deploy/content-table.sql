-- Deploy huni-backend:content-table to pg
-- requires: schema

BEGIN;

CREATE TABLE huni.content (
    id              text NOT NULL,
    version         integer NOT NULL,

    user_id         integer NOT NULL REFERENCES huni.user(id)
                    ON UPDATE CASCADE ON DELETE RESTRICT DEFERRABLE,

    content         bytea NOT NULL,

    created_utc     timestamp NOT NULL DEFAULT (now() at time zone 'utc'),

    PRIMARY KEY (id, version)
);

COMMIT;
