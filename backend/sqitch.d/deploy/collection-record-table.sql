-- Deploy collection-record-table
-- requires: collection-table

BEGIN;

SET client_min_messages = 'warning';

CREATE TABLE huni.collection_record (
    collection_id   INTEGER NOT NULL REFERENCES huni.collection(id)
                    ON UPDATE CASCADE ON DELETE CASCADE DEFERRABLE,

    docid           text NOT NULL
                    CONSTRAINT nonempty_docid CHECK (length(btrim(docid)) > 0),
    created_utc     timestamp NOT NULL DEFAULT (now() at time zone 'utc'),

    PRIMARY KEY (collection_id, docid)
);


COMMIT;
