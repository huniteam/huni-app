-- Deploy huni-backend:user-auth-extra-column to pg
-- requires: user-table

BEGIN;

ALTER TABLE huni.user
  ADD COLUMN auth_extra JSON DEFAULT NULL;

COMMIT;
