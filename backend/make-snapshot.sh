#!/bin/bash

# This script will create a new cpanfile.snapshot

docker run -it --rm \
    -v $PWD/cpanfile:/opt/backend/cpanfile \
    -v $PWD/cpanfile.snapshot:/opt/backend/cpanfile.snapshot.new \
    -w /opt/backend \
    perl:5.24 \
    bash -c " \
        apt-get update \
        && DEBIAN_FRONTEND=noninteractive apt-get install -y \
                netcat \
                postgresql-client \
        && cpanm -q -n Carton \
        && rm -f cpanfile.snapshot \
        && carton install --path /opt/carton \
        && cat cpanfile.snapshot > cpanfile.snapshot.new \
    "
