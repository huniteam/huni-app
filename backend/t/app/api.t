use HuNI::Backend::Test;

# $agent is logged in via the session.
my $agent = fixture->make_agent(name('User'));

# Create an api key for this user
my $api_info = resp_is(200, $agent->post('/apikey'));

for my $key (qw( api_username api_key )) {
    ok($api_info->{$key}, "API info has $key");
}

# $api uses the api credentials.
my $api = fixture->make_api_agent(
    $api_info->{api_username}, $api_info->{api_key});

eq_or_diff(resp_is(200, $agent->get('/profile')),
           resp_is(200, $api->get('/profile')),
    'Session user & api-key user see same profile');

done_testing();
