use HuNI::Backend::Test;

# 3 collections X, Y, Z
# X is public, Y is private, Z is private w/role
# Name records after the collections they're in.

# RX RY RZ RXY RYZ RXZ RXYZ

use Function::Parameters qw( :strict );

run_tests();
done_testing;
exit;

#---

sub run_tests {
    my %users = map { $_ => fixture->make_user($_) } (qw( owner user ));
    my %collections = create_collections(\%users);
    my @records = create_records(\%users, \%collections);

    my $agent = fixture->make_agent($users{user});

    my $query = join('&', map { "d=$_" } @records);
    my $json = resp_is(200, $agent->get("/record/collection?$query"));

    my $cx = collection_info($collections{X});
    my $cz = collection_info($collections{Z});
    eq_or_diff($json, {
        RX   => [ $cx ],
        RZ   => [ $cz ],
        RXY  => [ $cx ],
        RXZ  => [ $cx, $cz ],
        RYZ  => [ $cz ],
        RXYZ => [ $cx, $cz ],
    }, 'Found all record collections');

    # The logged-in user should see all collections with X or Z
    for my $record (@records) {
        my $json = resp_is(200, $agent->get("/record/$record/collection"));
        my $got = [ map { $_->{name} } @$json ];
        my $expected = [ grep { $_ eq 'X' || $_ eq 'Z' } split(//, $record) ];
        eq_or_diff($got, $expected, "Got collections for $record");
    }

    # The anon user should only see collections with X (public ones)
    my $anon = fixture->make_agent;
    for my $record (@records) {
        my $json = resp_is(200, $anon->get("/record/$record/collection"));
        my $got = [ map { $_->{name} } @$json ];
        my $expected = [ grep { $_ eq 'X' } split(//, $record) ];
        eq_or_diff($got, $expected, "Got collections for $record");
    }
}

#---

fun create_collections($users) {
    my $owner_id = $users->{owner}->{user_id};
    my $user_id  = $users->{user}->{user_id};
    my %role = (
        role_id => 'owner',
        inviting_user_id => $owner_id,
    );

    my %collections;
    my $rs = fixture->resultset('Collection');
    for my $name (qw( X Y Z )) {

        my $collection = $rs->create({ name => $name, is_public => 0 });
        $collection->add_to_users({ id => $owner_id }, \%role);

        $collections{$name} = $collection;
    }
    $collections{X}->update({ is_public => 1 });
    $collections{Z}->add_to_users({ id => $user_id }, \%role);

    return %collections;
}


fun create_records($users, $collections) {
    my $owner_id = $users->{owner}->{user_id};

    my @records;
    for my $code (qw( X Y Z XY YZ XZ XYZ )) {
        my $docid = "R$code";
        for my $name (split(//, $code)) {
            $collections->{$name}->add_to_collection_records({
                docid   => $docid,
                user_id => $owner_id,
            });
        }
        push(@records, $docid);
    }

    return @records;
}

fun collection_info($collection) {
    return {
        id          => $collection->id,
        name        => $collection->name,
        is_public   => $collection->is_public,
    };
}

#---
