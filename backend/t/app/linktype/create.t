use HuNI::Backend::Test;

my $agent = fixture->make_agent(name('User'));
my $other = fixture->make_agent(name('User'));

my ($from_type, $to_type) = qw( FROMTHING TOTHING );

# use bogus types for this so we don't get polluted by existing link types
HuNI::Backend::WebApp::Handler::LinkType::add_valid_entity_type($_)
    for (qw( FROMTHING TOTHING ));

clear_testing_types();

note 'Check basic creation'; {
    my $data = make_data(1);
    resp_is(200, $agent->post('/linktype', $data));

    my $linktypes = resp_is(200, $agent->get("/linktype/$from_type/$to_type"));

    is(@$linktypes, 1, 'one linktype');
    my $linktype = $linktypes->[0];

    my $pairtypes = resp_is(200, $agent->get("/linktype/$to_type/$from_type"));
    is(@$pairtypes, 1, 'one pairtype');
    my $pairtype = $pairtypes->[0];

    is($linktype->{id}, $pairtype->{pair_id}, 'link->id == pair->pair_id');
    is($pairtype->{id}, $linktype->{pair_id}, 'pair->id == link->pair_id');
    is($linktype->{name}, $data->{name},      'link->name == data->name');
    is($pairtype->{pair}, $data->{name},      'pair->pair == data->name');
    is($linktype->{pair}, $data->{pair},      'link->pair == data->pair');
    is($pairtype->{name}, $data->{pair},      'pair->name == data->pair');
}

note 'Check duplicates return the previous linktype'; {
    my $data = make_data(2);

    my $first = resp_is(200, $agent->post('/linktype', $data));

    # First time with the same user
    my $again = resp_is(200, $agent->post('/linktype', $data));

    eq_or_diff($first, $again);

    # This time with a different user
    $again = resp_is(200, $other->post('/linktype', $data));
    eq_or_diff($first, $again);
}

note 'Check swapped duplicates also return the previous linktype'; {
    my $data = make_data(3);

    my $first = resp_is(200, $agent->post('/linktype', $data));
    swap($data, 'name', 'pair');
    swap($data, 'from', 'to');

    my $again = resp_is(200, $other->post('/linktype', $data));

    is($first->{id}, $again->{pair_id}, 'same ids');
}

note 'Check non-empty constraints'; {
    my %good_data = %{ make_data(4) };

    for my $field (qw( name pair )) {
        for my $value ('', ' ') {
            my %data = %good_data;
            $data{$field} =  $value;
            resp_is(404, $agent->post('/linktype', \%data));
        }
    }
}

note 'Check bad entity types'; {
    my %good_data = (
        name => 'held at',
        pair => 'site of',
        from => 'Event',
        to   => 'Place',
    );

    for my $field (qw( from to )) {
        my %data = %good_data;
        $data{$field} = 'Feeb';
        resp_is(404, $agent->post('/linktype', \%data));
    }
}

clear_testing_types();

done_testing();

fun swap($hash, $k0, $k1) {
    my $t = $hash->{$k0};
    $hash->{$k0} = $hash->{$k1};
    $hash->{$k1} = $t;
}

fun make_data($test_id) {
    return {
        name => "Test $test_id forwards",
        pair => "Test $test_id reverse",
        from => $from_type,
        to   => $to_type,
    };
}

sub clear_testing_types {
    use HuNI::Backend::WebApp::Util qw( neo4j );

    my $params = {
        from => $from_type,
        to   => $to_type,
    };

    my $result = neo4j->do(<<'...', parameters => $params);
MATCH (linktype:Linktype)
WHERE (linktype.from = { from } AND linktype.to   = { to })
   OR (linktype.to   = { from } AND linktype.from = { to })
DELETE linktype
...
}
