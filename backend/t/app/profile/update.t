use HuNI::Backend::Test;

my $agent = fixture->make_agent(name('User'));
my $profile = $agent->build(profile())->{profile};

my $last_modified = $profile->{modified_utc};
clear_timestamps($profile);

note 'Check the initial timestamp'; {
    my $ret = resp_is(200, $agent->get('/profile'));
    is($ret->{modified_utc}, $last_modified, 'Timestamp matches');
}

note 'Try some valid updates'; {
    my @updates = (
        [qw( collaboration_interests description email institutions interests
             name )],
        [qw( email institutions interests name )],
        [qw( collaboration_interests interests name )],
        [qw( interests name )],
        [qw( collaboration_interests )],
        [qw( description )],
        [qw( email )],
        [qw( institutions )],
        [qw( interests )],
        [qw( name )],
    );

    for my $fields (@updates) {
        my $update = update_fields(@$fields);

        my $ret = resp_is(200, $agent->put('/profile', $update));
        cmp_ok($last_modified, 'lt', $ret->{modified_utc},
                'Timestamp got updated');
        $last_modified = $ret->{modified_utc};
        clear_timestamps($ret);
        eq_or_diff($ret, $profile, 'return value as expected');
    }
}

note 'Try some invalid updates'; {
    for my $field (qw( auth_userid auth_domain blahblah id )) {
        my $ret = resp_is(200,
                    $agent->put('/profile', { $field => 'ignored' } ));

        is($last_modified, $ret->{modified_utc}, 'Timestamp not updated');
        $last_modified = $ret->{modified_utc};
        clear_timestamps($ret);

        eq_or_diff($ret, $profile, 'returned data unchanged');
    }
}

note 'Try setting an empty name'; {
    for my $value ('', ' ') {
        resp_is(404, $agent->put('/profile', { name => $value }));
    }
}

note 'Try a partially invalid update'; {
    my $update = update_fields(qw( email name ));
    $update->{id}          = 'blah';
    $update->{auth_userid} = 'whah';
    $update->{foo}         = 'bar';

    my $ret = resp_is(200, $agent->put('/profile', $update));
    cmp_ok($last_modified, 'lt', $ret->{modified_utc},
            'Timestamp got updated');
    $last_modified = $ret->{modified_utc};
    clear_timestamps($ret);
    eq_or_diff($ret, $profile, 'return value as expected');
}

note 'Try updating a profile anonymously'; {
    my $anon = fixture->make_agent;
    resp_is(403, $anon->put('/profile', { name => 'anon' }));
}

done_testing;

fun update_fields(@fields) {
    my %update;
    for my $field (@fields) {
        $update{$field} = $profile->{$field} .= 'x';
    }
    return \%update;
}

fun clear_timestamps($hash) {
    # We've already checked the timestamps. Clear them out so we can compare
    # the rest with eq_or_diff
    delete $hash->{$_} for qw( created_utc modified_utc );
}
