use HuNI::Backend::Test;

my $path = '/collection';
my $username = 'User000';

note 'Create two collections and delete one'; {
    my $agent = fixture->make_agent(++$username);
    my $collections = $agent->build([
        collection(3), collection(2)
    ]);

    my $deleted   = $collections->[0]->{collection};
    my $remaining = $collections->[1]->{collection};

    resp_is(200, $agent->del("$path/$deleted->{id}"));

    resp_is(404, $agent->get("$path/$deleted->{id}"));
    resp_is(200, $agent->get("$path/$remaining->{id}"));
}

note 'Try deleting a non-existent collection'; {
    my $agent = fixture->make_agent(++$username);
    resp_is(404, $agent->del('/collection/12345'));
}

note 'Try deleting a malformed collection'; {
    my $agent = fixture->make_agent(++$username);
    resp_is(404, $agent->del('/collection/badbadbad'));
}

done_testing();
