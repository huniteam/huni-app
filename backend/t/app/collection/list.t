use HuNI::Backend::Test;

my $agent = fixture->make_agent(name('User'));

my %collections = (
    'Collection One'    => [ ],
    'Collection Two'    => [qw( docA docB docC )],
    'Collection Three'  => [qw( docA docC docE docG )],
    'Collection Four'   => [qw( docE docG )],
    'Collection Five'   => [qw( docC )],
);

note 'Try listing the collections when there are none'; {
    my $json = resp_is(200, $agent->get('/collection'));
    eq_or_diff($json, [ ], 'Empty list returned');
}

note 'Now create some collections'; {

    my $data = $agent->build([
        map { collection(name => $_, records => $collections{$_}) }
            sort keys %collections
    ]);

    my $json = resp_is(200, $agent->get('/collection'));

    is(scalar @$json, scalar keys %collections, 'Got all collections');

    for my $collection (@$json) {
        my $name = $collection->{name};
        my $docs = $collections{$name};
        ok($docs, "Found collection $name");
        is($collection->{record_count}, scalar @$docs,
            '... with correct record count');
        isnt($collection->{modified_utc}, undef, '... and a timestamp');
    }

    my @unsorted_timestamps = map { $_->{modified_utc} } @$json;
    my @sorted_timestamps = reverse sort @unsorted_timestamps;
    eq_or_diff(\@unsorted_timestamps, \@sorted_timestamps,
        'Collections returned most-recent first');
}

done_testing();
