use HuNI::Backend::Test;

my $agent = fixture->make_agent(name('User'));
my $spec = collection(synopsis => name('Synopsis'), record_count => 3);
my $collection = $agent->build($spec)->{collection};

note 'Fetch an existing collection'; {

    my $ret = resp_is(200, $agent->get('/collection/' . $collection->{id}));

    for my $key (qw( name synopsis )) {
        is($ret->{$key}, $collection->{$key}, "$key is correct");
    }
    ok(! $ret->{is_public}, 'not public');
    is(scalar @{ $ret->{records} },
       scalar @{ $collection->{records} }, 'record count is correct');
}

note 'Fetch some bad collections'; {
    resp_is(404, $agent->get('/collection/8675309'));
    resp_is(404, $agent->get('/collection/badbadbad'));
}

done_testing();
