use HuNI::Backend::Test;

use Function::Parameters qw( :strict );

run_tests();
done_testing;
exit;

#---

sub run_tests {
    my %users;
    my %agents;

    for my $name (qw( owner user )) {
        my $user = fixture->make_user($name);
        $users{$name} = $user;
        $agents{$name} = fixture->make_agent($user);
    }

    my $collection = create_collection(\%users);
    my $base = '/collection/' . $collection->id . '/user';

    my $user_id  = $users{user}->{user_id}; # yo dawg
    my $owner_id = $users{owner}->{user_id};

    resp_is(404, $agents{owner}->put("$base/$user_id/badrole"),
        "Creating invitation with unknown role fails");

    resp_is(200, $agents{owner}->put("$base/$user_id/read-write"),
        'Owner created rw invitation');

    role_is(\%users, $collection->id,
            role_id => 'read-write',
            msg => 'Found read-write role');

    resp_is(200, $agents{user}->del($base), 'User deleted own role');

    no_role_exists(\%users, $collection->id, 'User role is gone');

    resp_is(200, $agents{owner}->put("$base/$user_id/read-only"),
        'Owner created ro invitation');

    role_is(\%users, $collection->id,
            role_id => 'read-only',
            msg => 'Found read-only invitation');

    resp_is(200, $agents{owner}->del("$base/$user_id"),
        'Owner deleted ro invitation');

    no_role_exists(\%users, $collection->id, 'User role is gone');

    resp_is(404, $agents{owner}->del($base),
        'Owner cannot remove their own role unless other owners exist');

    resp_is(200, $agents{owner}->put("$base/$user_id/owner"),
        'Owner created owner invitation');

    role_is(\%users, $collection->id,
            role_id => 'owner',
            msg => 'Found owner invitation');

    resp_is(200, $agents{owner}->del($base),
        'Owner removes their own role now that other owner has accepted');

    resp_is(404, $agents{user}->del($base),
        'Other user cannot remove their role now b/c they are the only owner');
}

#---

fun create_collection($users) {
    my $owner_id = $users->{owner}->{user_id};

    my $rs = fixture->resultset('Collection');
    my $collection = $rs->create({ name => 'collection' });
    $collection->add_to_users(
        {
            id => $owner_id,
        },
        {
            role_id => 'owner',
            inviting_user_id => $owner_id,
        },
    );

    return $collection;
}

fun find_role($user_id, $collection_id) {
    my $role = fixture->resultset('CollectionUser')->search({
        collection_id => $collection_id,
        user_id       => $user_id,
    })->single;

    return $role ? $role->for_json : { };
}

fun make_role($users, $collection_id,
        :$user_id           = $users->{user}->{user_id},
        :$inviting_user_id  = $users->{owner}->{user_id},
        :$role_id           = 'owner') {

    return {
        user_id          => $user_id,
        collection_id    => $collection_id,
        inviting_user_id => $inviting_user_id,
        role_id          => $role_id,
    };
}

fun role_is($users, $collection_id, :$msg = undef, %role) {
    local $Test::Builder::Level = $Test::Builder::Level + 1;

    my $got = find_role($users->{user}->{user_id}, $collection_id);
    my $expected = make_role($users, $collection_id, %role);

    eq_or_diff($got, $expected, $msg);
}

fun no_role_exists($users, $collection_id, $msg = undef) {
    local $Test::Builder::Level = $Test::Builder::Level + 1;

    my $got = find_role($users->{user}->{user_id}, $collection_id);
    my $expected = { };

    eq_or_diff($got, $expected, $msg);
}

#---
#---
