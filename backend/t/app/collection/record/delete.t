use HuNI::Backend::Test;

my $agent = fixture->make_agent(name('User'));

note 'Create a collection and add some docs';
my @docids = qw( docX docY docZ );
my $collection = $agent->build(collection(records => \@docids))->{collection};
my $path = '/collection/' . $collection->{id};

note "Remove some docids";
my $record_count = scalar @docids;
for my $docid (@docids) {
    my $prev = $collection;
    $collection = resp_is(200, $agent->del("$path/$docid"));
    --$record_count;

    is($collection->{record_count}, $record_count,
        "$record_count records remain");
    cmp_ok($prev->{modified_utc}, 'lt', $collection->{modified_utc},
        "Timestamp got updated");
}

note 'Try removing a non-existent doc from a real collection'; {
    my $prev = $collection;
    $collection = resp_is(200, $agent->del("$path/$docids[0]"));
    is($prev->{modified_utc}, $collection->{modified_utc},
        "Timestamp didn't get updated");
    is($collection->{record_count}, 0, 'Still zero docs');
}

note 'Try removing docs from a non-existent collection'; {
    resp_is(404, $agent->del('/collection/12345/docA'));
}

note 'Try removing docs from a nonsensical collection'; {
    resp_is(404, $agent->del('/collection/badbadbad/docA'));
}

my $other_agent = fixture->make_agent(name('User'));
my $other_collection = $other_agent->build(collection(1))->{collection};
my $other_docid = $other_collection->{records}->[0]->{docid};

note 'Try removing docs from another users collection'; {

    TODO: {
        local $TODO = 'Need to change exists-but-not-yours to 403';
        resp_is(403,
            $agent->del("/collection/$other_collection->{id}/$other_docid"));
    };
}

note 'Try removing non-existent docs from another users collection'; {
    resp_is(404, $agent->del("/collection/$other_collection->{id}/nonono"));
}

note 'Try anonymously removing docs from a collection'; {
    my $anon_agent = fixture->make_agent;
    TODO: {
        local $TODO = 'Need to change exists-but-not-yours to 403';
        resp_is(403, $anon_agent->del(
            "/collection/$other_collection->{id}/$other_docid"));
    }
}

done_testing();
