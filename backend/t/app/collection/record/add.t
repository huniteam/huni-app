use HuNI::Backend::Test;

my $agent = fixture->make_agent(name('User'));

note 'Create a collection and add some docs';
my $collection = $agent->build(collection())->{collection};
my $path = '/collection/' . $collection->{id};

my ($json, $prev);

$json = resp_is(200, $agent->put("$path/docA"));
is($json->{record_count}, 1, 'One record in result');

cmp_ok($collection->{modified_utc}, 'lt', $json->{modified_utc},
    'Modified timestamp got updated');

note 'Add another doc';
$prev = $json;

$json = resp_is(200, $agent->put("$path/docB"));
is($json->{record_count}, 2, 'Two records in result');

cmp_ok($prev->{modified_utc}, 'lt', $json->{modified_utc},
    'Modified imestamp got updated');

note 'Add the same doc again';
$prev = $json;

$json = resp_is(200, $agent->put("$path/docB"));
is($json->{record_count}, 2, 'Still two records in result');

is($prev->{modified_utc}, $json->{modified_utc}, 'Timestamp unchanged');

note 'Try adding an empty record to a collection'; {
    resp_is(404, $agent->put("$path/%20"));
}

note 'Try adding docs to a non-existent collection'; {
    resp_is(404, $agent->put('/collection/12345/docA'));
}

note 'Try adding docs to a nonsensical collection'; {
    resp_is(404, $agent->put('/collection/badbadad/docA'));
}

note 'Try adding docs to another users collection'; {
    my $other_agent = fixture->make_agent(name('User'));
    my $other_collection = $other_agent->build(collection(1))->{collection};

    TODO: {
        local $TODO = 'Need to change exists-but-not-yours to 403';
        resp_is(403,
            $agent->put('/collection/' . $other_collection->{id} . '/docA'));
    };
}

note 'Try anonymously adding docs to a collection'; {
    my $anon_agent = fixture->make_agent;
    resp_is(403, $anon_agent->put("$path/docD"));
}


done_testing();
