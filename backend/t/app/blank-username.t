use HuNI::Backend::Test;

# Nina was getting crashes when she tried to sign up because her google
# account had no name set, and we insist on a name. Check that we handle
# that situation.

note 'Check user with name and email'; {
    my $username = 'good';
    my $email = "$username\@mail.com";
    my $agent = make_plack_agent($username, $email);
    my $resp = resp_is(200, $agent->get('/user'));
    is($resp->{display_name}, $username, 'display_name matches');
}

note 'Check user with name and no email'; {
    my $username = 'good';
    my $email = '';
    my $agent = make_plack_agent($username, $email);
    my $resp = resp_is(200, $agent->get('/user'));
    is($resp->{display_name}, $username, 'display_name matches');
}

note 'Check user with email but no name'; {
    for my $username ('', '  ', undef) {
        my $email = 'username@mail.com';
        my $agent = make_plack_agent($username, $email);
        my $resp = resp_is(200, $agent->get('/user'));
        my $expected = 'username';
        is($resp->{display_name}, $expected, 'display_name matches');
    }
}

note 'Check user with no email or username'; {
    my $username = '';
    my $email = '';
    my $agent = make_plack_agent($username, $email);
    my $resp = resp_is(200, $agent->get('/user'));
    my $user = $agent->user;
    my $expected = 'Worker Bee';
    is($resp->{display_name}, $expected, 'display_name is "$provider-$id"');
}

done_testing();

fun make_plack_agent($name, $email) {
    return HuNI::Backend::Test::Agent->new(
        app => fixture->app,
        session => {
            'oauth2-user' => {
                id => name('id'),
                provider => 'testing',
                name => $name,
                email => $email,
            },
        },
    );
}
