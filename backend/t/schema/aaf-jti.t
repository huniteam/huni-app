use HuNI::Backend::Test;

my $aaf_jti = fixture->resultset('AafJti');

my $now = time;
my $old = $now - 100;
my $new = $now + 100;

my $n = 2;
lives_ok {
    for (1 .. $n) {
        $aaf_jti->create({ jti => "old$_", expiry => $old });
        $aaf_jti->create({ jti => "new$_", expiry => $new });
    }
} 'Insert some fake jti keys';

is($aaf_jti->count, $n * 2, 'All jtis in table');

# Attempt to insert a jti that matches one of the old jtis.
# First the old jtis will be removed, so this should succeed.
lives_ok {
    $aaf_jti->validate('old1', $old);
} 'Expired duplicate key validated okay';

# We are now left with the new jtis, plus this recent one.
is($aaf_jti->count, $n + 1, 'All jtis in table');

# Now attempt to insert a jti that matches one of the new jtis.
# This should fail. Also, the previous (expired) jti should be removed.
throws_ok {
    $aaf_jti->validate('new1', $new);
} qr/unique constraint/, 'Attempt to insert duplicate jti fails';

done_testing();
