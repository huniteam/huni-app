use HuNI::Backend::Test;

my $users = fixture->schema->resultset('User');

my $auth = {
    id       => '1234',
    provider => 'abcd',
    name     => 'user1234',
    email    => 'user1234@email.com',
};

my $first;
lives_ok {
    $first = $users->find_or_create_from_oauth($auth);
} 'created a new user from auth details';
ok($first->{new_user}, 'New user is new');

my $again;
lives_ok {
    $again = $users->find_or_create_from_oauth($auth);
} 'looked up that user again';
ok(!$again->{new_user}, 'Returning user is not new');

for my $key (qw( user_id display_name )) {
    is($first->{$key}, $again->{$key}, "$key is same for both");
}

# Now try with different details
$auth->{name} .= 'x';
$auth->{email} .= 'au';
lives_ok {
    $again = $users->find_or_create_from_oauth($auth);
} 'looked up that user again, with different name & email';

for my $key (qw( user_id display_name )) {
    is($first->{$key}, $again->{$key}, "$key is same for both");
}

$again = $users->find({ id => $first->{user_id} });
ok($again, 'Found user by user_id');


done_testing();
