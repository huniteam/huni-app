#!/bin/bash

# PGPASSWORD is set from the outside
export PGPORT=$PG_PORT_5432_TCP_PORT
export PGUSER=postgres
export PGDATABASE=hunitest
export PGHOST=$PG_PORT_5432_TCP_ADDR

export HUNI_TEST_PG="DBI:Pg:dbname=$PGDATABASE;host=$PGHOST;port=$PGPORT;user=$PGUSER;password=$PGPASSWORD"

export PERL5LIB=$PWD/lib:$PWD/t/lib:$PWD/local/lib/perl5

wait-for-port() {
    local host=$1;      shift
    local port=$1;      shift
    local timeout=$1;   shift

    local sleeptime=2
    local retries=$(( $timeout / $sleeptime ))

    for i in $( seq $retries ); do
        echo Waiting for $@ to start \(attempt $i of $retries\)
        if nc -z $host $port; then
            echo $@ is ready
            return 0
        fi
        sleep $sleeptime
    done
    echo $@ is not ready - aborting after $timeout seconds
    return 1
}

export DANCER_CONFDIR=$PWD
wait-for-port $PGHOST $PGPORT 30 PostgreSQL database
createdb
prove -r t
