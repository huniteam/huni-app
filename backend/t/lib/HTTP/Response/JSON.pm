package HTTP::Response::JSON;

use 5.16.0;
use warnings;
use base 'HTTP::Response';

use JSON qw( decode_json );
use Function::Parameters qw( :strict );

fun new ($class, $http_response) {
    return bless $http_response, $class;
}

method json() {
    return $self->{json} //= eval { decode_json($self->content) };
}

1;

__END__

=head1 NAME

HTTP::Response::JSON - HTTP::Response with extra json method

=head1 SYNOPSIS

    my $http_response = $ua->get('/something');
    my $json_response = HTTP::Response::JSON->new($http_response);

    my $data = $json_response->json;

=cut
