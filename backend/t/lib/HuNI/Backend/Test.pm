package HuNI::Backend::Test;

use 5.16.0;
use warnings;

use base qw(Exporter);
use DateTime qw( );
use Function::Parameters qw(:strict);

our @EXPORT = qw(
    fixture
    json_is json_has
    resp_is

    timestamp_now row_timestamp
);

use Import::Into;

# Import::Into requires us to 'use' the modules we want to inject into the
# caller.
# We should specify an empty import list unless we plan to use them ourselves.
use Test::Most;
use Test::FailWarnings;
use HuNI::Backend::Test::Fixture;
use HTTP::Request::Common qw( DELETE POST PUT GET );
use Data::Dumper::Concise qw( Dumper );
use HuNI::Backend::Test::Builder::Shortcuts;

sub import {
    # Import a bunch of modules into the caller - this is exactly as if the
    # caller 'use'd them directly.
    shift->export_to_level(1);

    warnings->import::into(1);
    strict->import::into(1);
    feature->import::into(1, qw( say ));
    Test::Most->import::into(1);
    Test::FailWarnings->import::into(1);
    HuNI::Backend::Test::Fixture->import::into(1);
    HTTP::Request::Common->import::into(1);
    Function::Parameters->import::into(1, ':strict');
    HuNI::Backend::Test::Builder->import::into(1);
    HuNI::Backend::Test::Builder::Shortcuts->import::into(1);
}

use JSON qw( decode_json encode_json );
use HTTP::Response::JSON;
use Time::HiRes;

my $fixture;
sub fixture {
    $fixture //= HuNI::Backend::Test::Fixture->new;
}

fun resp_is($expected_code, $response, $msg = undef) {
    # Make test failures appear in the caller, not here.
    local $Test::Builder::Level = $Test::Builder::Level + 1;

    if (!defined $msg) {
        my $request = $response->request;
        my $method = $request->method;
        my $uri = $request->uri;
        my $data = $request->content;
        my $args = "'$uri'";
        $args .= ", '$data'" if $data ne '';
        $msg = "$method($args) returned $expected_code";
    }
    is($response->code, $expected_code, $msg);
    return $response->json;
}

my $datetime_format = '%FT%T.%3N Z';

sub timestamp_now {
    return DateTime->from_epoch(epoch => Time::HiRes::time)
                   ->strftime($datetime_format);
}

1;

__END__

=head1 NAME

HuNI::Backend::Test

=head1 DESCRIPTION

Module to be included by all HuNI::Backend tests. Provides test fixtures.

=head1 SYNOPSIS

# For database schema tests
    use HuNI::Backend::Test;

    # Get the DBIC schema - this creates and deploys a fresh database
    my $schema = fixture->schema;

    ...

    done_testing();

# For dancer application tests

    use HuNI::Backend::Test;

    # Create an agent - this creates a new database, and a new dancer2 app.
    # The agent is a Plack::Test object.
    my $agent = fixture->agent;

    fixture->login(%$user);
    my $res = $agent->request(GET "/isloggedin");
    json_is($res, $user, 'Got user details');

    ...

    done_testing();

=head1 EXPORTS

This module exports everything from:

=over

=item Test::Most

=item Test::FailWarnings

=item HuNI::Backend::Test::Fixture

=item HuNI::Backend::Test::Fixture::Database

=item HTTP::Request::Common

=back

Other methods are

=head2 fixture

Lazily create and return a single instance of HuNI::Backend::Test::Fixture

=head2 json_is ( $response, $expected_data, $msg )

Check that the response succeeded and the decoded json data matches the
data structure in $expected_data.

=head2 json_has ( $response, $expected_data, $msg, @fields )

Like json_is, but only the specified fields must match.
If no fields are specifed, the keys of %$expected_data are used.

=head2 get ( $path )

=head2 get_ok ( $path )

=head2 get_is ( $code, $path )

Perform get request and optionally check the return code.
Returns an L<HTTP::Response::JSON> object.

=head2 put ( $path, $data? )

=head2 put_ok ( $path, $data? )

=head2 put_is ( $code, $path, $data? )

Perform put request and optionally check the return code.
The data parameter is an optional hashref.
Returns an L<HTTP::Response::JSON> object.

=head2 post ( $path, $data? )

=head2 post_ok ( $path, $data? )

=head2 post_is ( $code, $path, $data? )

Perform post request and optionally check the return code.
The data parameter is an optional hashref.
Returns an L<HTTP::Response::JSON> object.

=head2 del ( $path, $data? )

=head2 del_ok ( $path, $data? )

=head2 del_is ( $code, $path, $data? )

Perform delete request and optionally check the return code.
The data parameter is an optional hashref.
Returns an L<HTTP::Response::JSON> object.
=cut
