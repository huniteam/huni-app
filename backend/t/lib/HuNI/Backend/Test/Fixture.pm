package HuNI::Backend::Test::Fixture;
use 5.16.0;
use warnings;

BEGIN { $ENV{PLACK_ENV} = 'testing'; }
use HuNI::Backend::WebApp;
use HuNI::Backend::Schema;
use HuNI::Backend::SolrInterface;
use HuNI::Backend::WebApp::Util qw( set_schema set_solr_select set_solr_update );

use REST::Neo4j             qw( );
use REST::Neo4j::Schema     qw( );

use HTTP::Request::Common;
use HuNI::Backend::Test::Agent;
use HuNI::Backend::Test::Fixture::Database;
use Plack::Test;
use Moo;
use Function::Parameters ':strict';

has database => (
    is => 'lazy',
    builder => sub { HuNI::Backend::Test::Fixture::Database->new },
    handles => [qw( schema dsn )],
);

has neo => (
    is => 'lazy',
    builder => method() {
        REST::Neo4j->new(base_url => $ENV{SD_NEO4J_URI} . 'db/data/')
    },
);

sub BUILD {
    my $neo = REST::Neo4j->new(base_url => $ENV{SD_NEO4J_URI} . 'db/data/');
    my $schema = REST::Neo4j::Schema->new(neo => $neo, config => 'neo4j-schema.yaml');

    # Delete everything in the database, we want to start fresh.
    $schema->revert_range($schema->deployed_version, 0);
    $neo->do('MATCH (x)-[y]-(z) DELETE x, y, z');

    # And deploy.
    $schema->deploy;
}

method resultset($name) { return $self->schema->resultset($name) }

{
    package Mock::HTTP::Tiny;
    sub new { bless {}, shift }
    sub post { return { success => 1 } }
}

{
    package Mock::Solr::Select;
    sub new { bless {}, shift }
    sub find_docids {
        shift;
        return { map { $_ => { docid => $_, name => $_ } } @_ };
    }
}

has app => (
    is => 'lazy',
    builder => method() {
        set_schema(HuNI::Backend::Schema->connect($self->dsn));
        my $ua = Mock::HTTP::Tiny->new;
        set_solr_update(HuNI::Backend::SolrInterface->new(ua => $ua, endpoint => 'x'));
        set_solr_select(Mock::Solr::Select->new);
        return HuNI::Backend::WebApp->dance;
    },
);

method make_user($display_name) {
    # Insert this user into the database if they don't already exist.
    return $self->schema->resultset('User')
                ->find_or_create_from_oauth({
                    name => $display_name,
                    id   => $display_name,
                    provider => 'testing',
                });
}

method make_agent($user = undef) {
    if ($user && !ref $user) {
        $user = $self->make_user($user);
    }
    return HuNI::Backend::Test::Agent->new(app => $self->app, user => $user)
}

method make_api_agent($api_username, $api_key) {
    return HuNI::Backend::Test::Agent->new(
        app => $self->app, api_username => $api_username, api_key => $api_key);
}

1;

__END__

=head1 NAME

HuNI::Backend::Test::Fixture - fixture class for HuNI::Backend::WebApp testing

=cut
