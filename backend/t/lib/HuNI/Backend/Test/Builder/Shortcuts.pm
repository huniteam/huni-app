package HuNI::Backend::Test::Builder::Shortcuts;
use 5.16.0;
use warnings;

use base 'Exporter';
use Function::Parameters qw( :strict );

our @EXPORT = qw(
    name
    collection link linktype profile
);

my $next_id = 1000;
fun name($prefix) {
    return $prefix . $next_id++;
}

sub collection {
    my %args;
    if (@_ == 1) {
        $args{record_count} = $_[0];
    }
    else {
        %args = @_;
    }
    if (exists $args{record_count}) {
        my $record_count = delete $args{record_count};
        $args{records} = [ map { name('Record') } (1 .. $record_count) ];
    }
    $args{name} //= name('Collection');
    return { collection => \%args };
}

fun profile(%args) {
    my @columns =
        qw( collaboration_interests description email institutions interests );

    for my $column (@columns) {
        if (!exists $args{$column}) {
            $args{$column} = name($column);
        }
    }

    return { profile => \%args };
}

fun linktype($from, $to, :$name = name($from . '->' . $to),
                         :$pair = name($to . '->' . $from)) {

    return {
        linktype => {
            from => $from,
            to   => $to,
            name => $name,
            pair => $pair,
        }
    };
}

fun link($from, $to, $linktype) {
    return {
        link => {
            from_docid  => $from,
            to_docid    => $to,
            linktype_id => $linktype->{id},
        }
    };
}

1;
