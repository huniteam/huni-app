package HuNI::Backend::Test::Builder;
use 5.16.0;
use warnings;

use Moo;
use Function::Parameters qw( :strict );
use Data::Dumper::Concise;

has agent => (
    is => 'ro',
    required => 1,
);

my %handler_table = (
    collection => \&_build_collection,
    link       => \&_build_link,
    linktype   => \&_build_linktype,
    profile    => \&_build_profile,
);

method process($data) {
    my $type = ref $data;

    if ($type eq '') {
        return $data;
    }

    if ($type eq 'ARRAY') {
        return [ map { $self->process($_) } @$data ];
    }

    if ($type ne 'HASH') {
        die 'Unexpected data type: ' . Dumper($type);
    }

    my %ret;
    for my $key (keys %$data) {
        my $handler = $handler_table{$key};
        die 'Unexpected data: ' . Dumper($data) unless $handler;
        $ret{$key} = $self->$handler($data->{$key});
    }
    return \%ret;
};

method _build_collection($data) {
    my $path = '/collection';
    my @create_keys = qw( name );
    my @update_keys = qw( is_public synopsis );

    my $collection = $self->_post($path, _slice($data, @create_keys));
    $path .= '/' .  $collection->{id};
    if (my $update = _slice($data, @update_keys)) {
        $collection = $self->_put($path, $update);
    }
    for my $docid (@{ $data->{records} // [] }) {
        $collection = $self->_put("$path/$docid");
    }
    $collection->{records} = $self->_get("$path/record");
    return $collection;
}

method _build_linktype($data) {
    my $path = '/linktype';
    my @create_keys = qw( from to name pair );
    my $linktype = $self->_post($path, _slice($data, @create_keys));
    return $linktype;
}

method _build_link($data) {
    my $path = '/link';
    my @create_keys = qw( from_docid to_docid linktype_id );
    my $link = $self->_post($path, _slice($data, @create_keys));
    return $link;
}

method _build_profile($data) {
    my $path = '/profile';
    my @update_keys = qw( collaboration_interests description email
                           institutions interests name );
    my $update = _slice($data, @update_keys);
    my $profile = $self->_put("$path", $update);
    return $profile;
}

method _get(@args)  { $self->_req(GET  => @args) }
method _post(@args) { $self->_req(POST => @args) }
method _put(@args)  { $self->_req(PUT  => @args) }

method _req($method, $path, $data = undef) {
    my $response = $self->agent->request($method, $path, $data);
    if (! $response->is_success) {
        my $msg = "$method $path FAILED\n";
        if ($data) {
            $msg .= "DATA: " . Dumper($data) . "\n";
        }
        $msg .= "RESP: " . Dumper($response) . "\n";
        die $msg;
    }
    return $response->json;
}

fun _slice($hash, @keys) {
    my @has_keys = grep { exists $hash->{$_} } @keys;
    return unless @has_keys;
    return { map { $_ => $hash->{$_} } @has_keys };
}

1;
