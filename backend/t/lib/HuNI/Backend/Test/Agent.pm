package HuNI::Backend::Test::Agent;
use 5.16.0;
use warnings;

use JSON qw( encode_json );
use Moo;
use Plack::Test;
use HTTP::Request::Common qw( DELETE POST PUT GET );
use HTTP::Response::JSON ();
use HuNI::Backend::Test::Builder;

use Function::Parameters ':strict';

has app => (
    is => 'ro',
    required => 1,
);

has agent => (
    is => 'lazy',
);

has user => (
    is => 'ro',
    predicate => 1,
);

has session => (
    is => 'ro',
    predicate => 1,
);

has [qw( api_username api_key )] => (
    is => 'ro',
    predicate => 1,
);

method _build_agent() {
    my $app = $self->has_user || $self->has_session
            ? $self->_wrapped_app : $self->app;
    return Plack::Test->create($app);
}

method _wrapped_app() {
    my $app = $self->app;
    my $session;

    if ($self->has_session) {
        # Use the custom session that was specified.
        $session = $self->session;
    }
    else {
        # Build a default login session from the username.
        $session = {
            'huni-user' => $self->user,
            'session-time' => time(),
        };
    };

    return fun($env) {

        # Quick and dirty plack middleware to inject a mock session

        # This is the actual session data
        $env->{'psgix.session'} = $session;

        # I don't *think* the cookie will be set here, but lets cack it
        # noisily if it is.
        die 'Unexpected HTTP_COOKIE already set: ' . $env->{HTTP_COOKIE}
            if exists $env->{HTTP_COOKIE};

        # We need plack_session to be present, but it doesn't need to
        # actually be anything.
        $env->{HTTP_COOKIE} = 'plack_session=ITS-A-FUGAZI';

        return $app->($env);
    };
}

method get($path) {
    return $self->request(GET => $path);
}

method post($path, $data = undef) {
    return $self->request(POST => $path, $data);
}

method put($path, $data = undef) {
    return $self->request(PUT => $path, $data);
}

method del($path) {
    return $self->request(DELETE => $path);
}

method request($method, $path, $data = undef) {
    my $request = do {
        no strict 'refs';
        $method->($path);
    };
    if (defined $data) {
        my $content = encode_json($data);
        $request->content($content);
        $request->content_length(length $content);
        $request->content_type('application/json');
    }
    if ($self->has_api_username) {
        $request->authorization_basic($self->api_username, $self->api_key);
    }
    my $response = $self->agent->request($request);
    return HTTP::Response::JSON->new($response);
}

method build($data) {
    my $builder = HuNI::Backend::Test::Builder->new(agent => $self);
    return $builder->process($data);
}

1;

__END__

=head1 NAME

HuNI::Backend::Test::Agent

=head1 DESCRIPTION

Wrapper around Plack::Test with an augmented C<request> method.

=head1 METHODS

=head2 request($method, $path, $data = undef)

Performs the request and returns an L<HTTP::Response::JSON> object.

=head3 Parameters

=over

=item C<$method>

One of C<'GET'>, C<'PUT'>, C<'POST'>, C<'DELETE'>

=item C<$path>

URI path, eg. C<'/login/user'>

=item C<$data>

Optional data structure suitable for conversion to JSON.

=cut
