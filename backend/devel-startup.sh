#!/bin/bash -e

cat sqitch.conf > $SQITCH_CONFIG
cat <<END_CONFIG >> $SQITCH_CONFIG
[target "$SQITCH_TARGET"]
	uri = db:pg://${POSTGRES_USER}:${POSTGRES_PASSWORD}@${PGHOST}/${POSTGRES_DB}
[engine "pg"]
	target = $SQITCH_TARGET
END_CONFIG

eval $( sqitch env )
export HUNI_BACKEND_DSN=$( sqitch dsn )

eval $( /fixture/postgres/wait.sh )
eval $( /fixture/neo4j/wait.sh )

# This is okay to commit - it's in the aaf testing zone, and only redirects
# back to localhost anyway.
export HUNI_BACKEND_AAF_CONF=/tmp/aaf.yml
cat <<END_AAF > $HUNI_BACKEND_AAF_CONF
aud: http://localhost:2000
iss: https://rapid.test.aaf.edu.au
request_uri: https://rapid.test.aaf.edu.au/jwt/authnrequest/research/Khdc37_nxTmhiv8cOWjLDA
secret: 'Y-iSZ7$+E4[+9<Abj@T2{:rG(E6xy3D:'
END_AAF

# This too
export HUNI_BACKEND_OAUTH_CONF=/tmp/oauth2.yml
cat <<END_OAUTH > $HUNI_BACKEND_OAUTH_CONF
fixture:
    client_id: fixture_id
    client_secret: fixture_secret
END_OAUTH

if [[ $# == 0 ]]; then
    # Default startup
    sqitch deploy
    ./tools/load-cms --dir cms --dsn "$HUNI_BACKEND_DSN"
    ./tools/update-neo4j --schema neo4j-schema.yaml deploy
    plackup -r -p 80 -s Starlet app.psgi
else
    # Custom command-line
    exec "$@"
fi
