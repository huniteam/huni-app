use Plack::Builder;
use HuNI::Backend::WebApp;

$ENV{HUNI_BACKEND_MOCK_USER} = 'HuNI Developer';

builder {
    enable 'Session', store => 'File';
    enable 'CrossOrigin',
        origins => '*',
        methods => '*',
        headers => '*',
        ;

    mount '/vlab' => HuNI::Backend::WebApp->dance;
}

__END__

=head1 NAME

dev.psgi

=head1 DESCRIPTION

Development PSGI file intended for running a development backend that can
be accessed from frontend running on your local machine.

The Auth::OAuth module is removed and replaced by a single fixed user via
the HUNI_BACKEND_MOCK_USER environment variable.

The CrossOrigin middleware is enabled to allow cross-origin requests from
localhost.

=cut
