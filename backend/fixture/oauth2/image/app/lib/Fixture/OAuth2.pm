package Fixture::OAuth2;

use 5.24.0;
use warnings;

use Function::Parameters qw( :strict );
use Dancer2;
use Data::Dumper::Concise;
use MIME::Base64;
use URI;

get '/authorize' => fun($app) {
    my $req = $app->request;
    my $params = $req->query_parameters;

#use Data::Dumper::Concise; print STDERR Dumper({ '/authorize' => $params });

    my $hidden_inputs = join("\n",
        map { qq(<input type="hidden" name="$_" value="$params->{$_}">) }
            $params->keys);

    my $html = <<"END";
        <html><body><form action="/login" method="post">
          <p>Welcome, HuNI development user.</p>
          <p>Log in with any username you like, user accounts get created on demand.</p>
          <input text="text" name="username" placeholder="Any username" />
          $hidden_inputs
          <input type="submit" value="Log in">
        </form></body></html>
END

    return $html;
};

post '/login' => fun($app) {
    my $req = $app->request;
    my $params = $req->body_parameters;

#use Data::Dumper::Concise; print STDERR Dumper({ '/login' => $params });

    my $uri = URI->new($params->{redirect_uri});
    $uri->query_form(
        code  => encode_base64($params->{username}),
        state => $params->{state},
    );

    $app->response->redirect($uri);
};

post '/token' => fun($app) {
    my $req = $app->request;
    my $params = $req->body_parameters;

#use Data::Dumper::Concise; print STDERR Dumper({ '/token' => $params });

    send_as JSON => { access_token => $params->{code} }
};

get '/user' => fun($app) {
    my $req = $app->request;
    my $params = $req->query_parameters;

#use Data::Dumper::Concise; print STDERR Dumper({ '/user' => $params });

    my $username = decode_base64($params->{access_token});
    send_as JSON => {
        id      => $username,
        name    => $username,
        email   => "$username\@localhost",
    };
};

any '/**' => fun($app) {
    my $req = $app->request;

    if ($req->path ne '/favicon.ico') {
        for (qw( method path content body_parameters query_string )) {
            print STDERR Dumper({ $_ => $req->$_ });
        }
    }
};

1;
