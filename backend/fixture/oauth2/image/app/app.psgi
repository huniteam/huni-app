use 5.24.1;
use warnings;
#use Log::Any::Adapter qw( FileHandle );
use Plack::Builder;
use Fixture::OAuth2;

builder {
    enable "ReverseProxy";
    enable "ReverseProxyPath";

    Fixture::OAuth2->psgi_app;
}
