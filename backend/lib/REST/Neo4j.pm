package REST::Neo4j;

use 5.22.0;
use warnings;

use Carp                    qw( croak );
use Function::Parameters    qw( :strict );
use Log::Any                qw( $log );
use Try::Tiny;

use Moo;
extends qw( REST::Base );

has '+headers' => (
    builder => sub { [ 'X-Stream' => 'true' ] },
);

has default_result_type => (
    is => 'lazy',
    builder => sub { [qw( row )] },
);

method do($statement, :$parameters = { },
                      :$transaction_id = 'commit',
                      :$result_type = undef) {

    $result_type //= $self->default_result_type;
    my $data = $self->post("transaction/$transaction_id", data => {
        statements => [
            {
                statement => $statement,
                parameters => $parameters,
                resultDataContents => $result_type,
            },
        ],
    });

    return $data->{results}->[0];
}

method begin() {
    my $data = $self->post('transaction');

    my ($transaction_id) = ($data->{commit} =~ m{transaction/(\d+)/commit$})
        or croak "Unexpected commit url: $data->{commit}\n";

    return $transaction_id;
}

method commit($transaction_id) {
    my $data = $self->post("transaction/$transaction_id/commit");
    return 1;
}

method rollback($transaction_id) {
    my $ok = 0;
    try {
        # This will die if the transaction has aborted already.
        $self->delete("transaction/$transaction_id");
        $ok = 1;
    };
    return $ok;
}

around request => fun($orig, $self, @args) {
    my $data = $self->$orig(@args);
    if (my $error = $data->{errors}->[0]) {
        if ($error->{code} =~ /ConstraintValidationFailed/) {
            die $error; # We need this for update_counter etc to work
        }

        # Other errors shuold be logged.
        $log->error($error->{message});
        croak "$error->{code}";
    }
    return $data;
};

if (0) {
    around post => fun($orig, $self, @args) {
        use Data::Dumper::Concise; print STDERR Dumper(POST => \@args);
        my $data = $self->$orig(@args);
        use Data::Dumper::Concise; print STDERR Dumper($data->{results}->[0]);
        return $data;
    };
}

1;
