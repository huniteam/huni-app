package Term::ReadLine::REPL;

use 5.22.0;
use warnings;

use Function::Parameters qw( :strict );
use Path::Tiny qw( );
use Term::ReadLine qw( );

use Moo;

has name         => ( is => 'ro', default => 'repl' );
has prompt       => ( is => 'ro', default => '> ' );
has history_file => ( is => 'ro', default => '.repl-history' );

has term => (
    is => 'lazy',
    builder => method() {
        my $term = Term::ReadLine->new($self->name);
        $term->addhistory($_) for $self->history->lines;
        return $term;
    },
);

has history => (
    is => 'lazy',
    builder => method() {
        Path::Tiny->new($self->history_file)->touch;
    },
);

method run($f) {
    while (defined ($_ = $self->term->readline($self->prompt))) {
        next unless /\S/;
        $f->($_, $self->term);
        $self->term->addhistory($_);
        $self->history->append($_);
    }
}

1;
