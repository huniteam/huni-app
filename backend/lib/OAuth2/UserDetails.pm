package OAuth2::UserDetails;

use strict;
use warnings;

use base 'Exporter';

use Carp qw( croak );
use HTTP::Tiny;
use Cpanel::JSON::XS qw( decode_json );
use Try::Tiny;

our @EXPORT = qw( get_details );

my %handler = (
    box         => \&get_box_details,
    dropbox     => \&get_dropbox_details,
    facebook    => \&get_facebook_details,
    github      => \&get_github_details,
    google      => \&get_google_details,
    instagram   => \&get_instagram_details,
    linkedin    => \&get_linkedin_details,
    microsoft   => \&get_microsoft_details,
    fixture     => \&get_fixture_details,
);

sub get_details {
    my %args = @_;

    my $provider = $args{provider}
        or croak("Missing parameter: provider");

    my $handler = $handler{$provider}
        or croak("Unknown provider: $provider. Known providers: "
                . join(', ', sort keys %handler));

    $handler->(@_);
}

sub get_box_details {
    my ($ua, $access_token) = _get_args(@_);

    my $response = $ua->get('https://api.box.com/2.0/users/me', {
        headers => {
            Authorization => "Bearer $access_token",
        },
    });
    my $content = decode_json($response->{content});
    if ($response->{success}) {
        return {
            provider => 'box',
            id      => $content->{id},
            name    => $content->{name},
            email   => $content->{login},
        };
    }
    else {
        die $content->{message};
    }
}

sub get_dropbox_details {
    my ($ua, $access_token, $provider_data) = _get_args(@_);

    # https://www.dropbox.com/developers/reference/migration-guide
    #
    # In v1, dropbox used a uid number to identify users, which is what we
    # have in the user database as the auth_userid.
    #
    # In v2, there is now an account_id, but the v2 API provides _no way_ of
    # finding the v1 uid. Terrific. What does happen is that the request token
    # call in the v2 OAuth2 sequence returns both the account_id and uid.
    #
    # Our OAuth2 workflow has been changed so that we can get the raw request
    # token response, which allows us to retrieve the old uid.

    my $uri = 'https://api.dropbox.com/2/users/get_current_account';
    my $response = $ua->post($uri, {
        headers => {
            Authorization => "Bearer $access_token",
        }
    });
    my $content = decode_json($response->{content});
    if ($response->{success}) {
        return {
            provider => 'dropbox',
            id       => $provider_data->{uid} // $content->{account_id},
            name     => $content->{name}->{display_name},
            email    => $content->{email} // '',
        };
    }
    else {
        die $content;
    }
}

sub get_facebook_details {
    my ($ua, $access_token) = _get_args(@_);

    my $uri = _make_uri($ua, 'https://graph.facebook.com/me', {
        access_token => $access_token,
    });
    my $response = $ua->get($uri);
    my $content = decode_json($response->{content});
    if ($response->{success}) {
        return {
            provider => 'facebook',
            id       => $content->{id},
            name     => $content->{name},
            email    => $content->{email},
        }
    }
    else {
        die $content;
    }
}

sub get_github_details {
    my ($ua, $access_token) = _get_args(@_);

    my $uri = _make_uri($ua, 'https://api.github.com/user', {
        access_token => $access_token,
    });
    my $response = $ua->get($uri);
    my $content = decode_json($response->{content});
    if ($response->{success}) {
        return {
            provider => 'github',
            id      => $content->{id},
            name    => $content->{name} // $content->{login},
            email   => $content->{email},
        };
    }
    else {
        die $content->{message};
    }
}

sub get_google_details {
    my ($ua, $access_token) = _get_args(@_);
    my $uri = _make_uri($ua, 'https://www.googleapis.com/plus/v1/people/me', {
        access_token => $access_token,
    });
    my $response = $ua->get($uri);

    my $content = decode_json($response->{content});
    if ($response->{success}) {
        return {
            provider => 'google',
            id      => $content->{id},
            name    => $content->{displayName},
            email   => $content->{emails}->[0]->{value},
        };
    }
    else {
        die $content->{message};
    }
}

sub get_instagram_details {
    my ($ua, $access_token) = _get_args(@_);
    my $uri = _make_uri($ua, 'https://api.instagram.com/v1/users/self', {
        access_token => $access_token,
    });

    my $response = $ua->get($uri);
    my $content = decode_json($response->{content});
    if ($response->{success}) {
        return {
            provider => 'instagram',
            id      => $content->{data}->{id},
            name    => $content->{data}->{full_name},
            email   => '',
        };
    }
    else {
        die $content->{message};
    }
}

sub get_linkedin_details {
    my ($ua, $access_token) = _get_args(@_);

    # First get the user details. This doesn't include email address.
    # https://docs.microsoft.com/en-us/linkedin/shared/integrations/people/profile-api?context=linkedin/consumer/context#retrieve-current-members-profile
    my $uri = _make_uri($ua, "https://api.linkedin.com/v2/me", {
        projection => '(id,localizedFirstName,localizedLastName)',
        oauth2_access_token => $access_token,
    });
    my $response = $ua->get($uri);
    my $content = decode_json($response->{content});

    my %details;
    if ($response->{success}) {
        $details{provider} = 'linkedin';
        $details{id} = $content->{id};

        my $name = join(' ', grep { defined } (
            $content->{localizedFirstName},
            $content->{localizedLastName},
        ));

        $details{name} = $name if length($name) > 0;
    }
    else {
        die $content->{message};
    }

    # Now ferret out the email address.
    # https://docs.microsoft.com/en-us/linkedin/shared/integrations/people/primary-contact-api#retrieve-email-address
    $uri = _make_uri($ua, "https://api.linkedin.com/v2/clientAwareMemberHandles", {
        q                   => 'members',
        projection          => '(elements*(primary,type,handle~))',
        oauth2_access_token => $access_token,
    });
    $response = $ua->get($uri);
    $content = decode_json($response->{content});
    if ($response->{success}) {
        my ($email) = grep { defined }
                      map { $_->{'handle~'}->{emailAddress} }
                      @{ $content->{elements} };

        if ($email) {
            $details{email} = $email;
        }
    }
    else {
        warn $content->{message};
    }

    return \%details;
}

sub get_microsoft_details {
    my ($ua, $access_token) = _get_args(@_);
    my $uri = _make_uri($ua, 'https://apis.live.net/v5.0/me', {
        access_token => $access_token,
    });

    my $response = $ua->get($uri);
    my $content = decode_json($response->{content});
    if ($response->{success}) {
        return {
            provider => 'microsoft',
            id      => $content->{id},
            name    => $content->{name},
            email   => '',
        };
    }
    else {
        die $content->{message};
    }
}

sub get_fixture_details {
    my ($ua, $access_token) = _get_args(@_);
    my $uri = _make_uri($ua, 'http://oauth2/user', {
        access_token => $access_token,
    });

    my $response = $ua->get($uri);
    my $content = decode_json($response->{content});
    if ($response->{success}) {
        return {
            provider => 'fixture',
            id      => $content->{id},
            name    => $content->{name},
            email   => $content->{email},
            extra   => {
                date => scalar localtime,
                pid  => $$,
                something => 'something',
            },
        };
    }
    else {
        die $content->{message};
    }
}


sub _get_args {
    my %args = @_;

    my $ua = $args{ua}
        or croak("Missing parameter: ua");

    my $access_token = $args{access_token}
        or croak("Missing parameter: access_token");

    return ($ua, $access_token, $args{provider_data});
}

sub _make_uri {
    my ($ua, $uri, $data) = @_;

    return $uri . '?' . $ua->www_form_urlencode($data);
}

1;

__END__

=head1 NAME

OAuth2::UserDetails - Fetch user details from an OAuth2 provider

=head1 DESCRIPTION

OAuth2 providers follow a standard protocol for authenticating a user, but this
provides only an access token, no actual user details.

This module works in conjunction with OAuth2::Simple to fetch the user details
given an access token.

=head1 SUPPORTED PROVIDERS

=over

=item box

box.com

=item github

GitHub

=item google

Google

=item linkedin

LinkedIn

=back

=head1 METHODS

=head2 get_details($provider, $access_token)

Look up the user details for the given provider and return these hash fields.

=over

=item provider

Same as the $provider parameter

=item id

Unique user id for this provider

=item name

Display name

=item email

User email (may be empty)

=back

=cut
