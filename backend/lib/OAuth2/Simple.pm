package OAuth2::Simple;

use strict;
use warnings;

use Carp qw( croak );
use HTTP::Tiny ();
use JSON qw( decode_json );

use namespace::clean;

my %provider = (
    # https://developers.box.com/oauth
    box => {
        name        => 'Box',
        auth_uri    => 'https://www.box.com/api/oauth2/authorize',
        token_uri   => 'https://www.box.com/api/oauth2/token',
    },
    # https://www.dropbox.com/developers/core/docs
    dropbox => {
        name        => 'Dropbox',
        auth_uri    => 'https://www.dropbox.com/oauth2/authorize',
        token_uri   => 'https://api.dropbox.com/oauth2/token',
    },
    # https://developers.facebook.com/docs/facebook-login/manually-build-a-login-flow/v2.0
    facebook => {
        name        => 'Facebook',
        auth_uri    => 'https://www.facebook.com/dialog/oauth',
        token_uri   => 'https://graph.facebook.com/oauth/access_token',
    },
    # https://developer.github.com/v3/oauth
    github => {
        name        => 'GitHub',
        auth_uri    => 'https://github.com/login/oauth/authorize',
        token_uri   => 'https://github.com/login/oauth/access_token',
    },
    # https://developers.google.com/accounts/docs/OAuth2WebServer
    google => {
        name        => 'Google',
        auth_uri    => 'https://accounts.google.com/o/oauth2/auth',
        token_uri   => 'https://accounts.google.com/o/oauth2/token',
        scope       => 'profile email',
    },
    # http://instagram.com/developer/authentication/#
    instagram => {
        name        => 'Instagram',
        auth_uri    => 'https://api.instagram.com/oauth/authorize',
        token_uri   => 'https://api.instagram.com/oauth/access_token',
    },
    # https://developer.linkedin.com/documents/authentication
    linkedin => {
        name        => 'LinkedIn',
        auth_uri    => 'https://www.linkedin.com/uas/oauth2/authorization',
        token_uri   => 'https://www.linkedin.com/uas/oauth2/accessToken',
        scope       => 'r_emailaddress r_liteprofile',
    },
    microsoft => {
        name        => 'Microsoft OneDrive',
        auth_uri    => 'https://login.live.com/oauth20_authorize.srf',
        token_uri   => 'https://login.live.com/oauth20_token.srf',
        scope       => 'wl.basic',
    },
    fixture => {
        name        => 'Local Testing Fixture',
        auth_uri    => 'http://localhost:2007/authorize',
        token_uri   => 'http://oauth2/token',
    },
);

sub new {
    my ($class, %args) = @_;

    my $self = { };
    if (exists $args{provider}) {
        my $p = delete $args{provider};
        if (! exists $provider{$p}) {
            croak("Unknown provider $p. Known providers: "
                    . join(', ', sort keys %provider));
        }
        $self = {
            %{ $provider{$p} },
            name => $p,
        },
    }
    else {
        croak("Missing parameter provider");
    }

    my @fields = qw( name auth_uri token_uri redirect_uri client_id client_secret );
    for my $field (@fields) {
        if (exists $args{$field}) {
            $self->{$field} = delete $args{$field};
        }
        elsif (!exists $self->{$field}) {
            croak("Missing parameter $field");
        }
    }
    $self->{ua} = delete $args{ua} || HTTP::Tiny->new;

    if (%args) {
        croak("Unexpected args: " . join(', ', keys %args));
    }

    bless $self, $class;
}

sub provider_info {
    return { %provider };
}

sub provider { $_[0]->{name} }

sub auth_uri {
    my ($self, $state) = @_;

    my %extras;
    $extras{scope} = $self->{scope} if defined $self->{scope};
    my $query = $self->{ua}->www_form_urlencode({
        response_type => 'code',
        client_id => $self->{client_id},
        state => $state,
        redirect_uri => $self->{redirect_uri},
        %extras,
    });
    return $self->{auth_uri} . '?' . $query;
}

sub request_token {
    my ($self, $code) = @_;

    my $data = {
        grant_type      => 'authorization_code',
        code            => $code,
        redirect_uri    => $self->{redirect_uri},
        client_id       => $self->{client_id},
        client_secret   => $self->{client_secret},
    };
    my $opts = {
        headers => {
            Accept => 'application/json',
        },
    };
    my $response = $self->{ua}->post_form($self->{token_uri}, $data, $opts);
    my $content = _decode_response($response);

    unless (exists $content->{access_token}) {
        die "request_token failed " . $content->{error} . $content->{error_description};
    }

    return $content;
}

sub _decode_response {
    my ($response) = @_;

    my $content_type = $response->{headers}->{'content-type'};
    my $content = $response->{content};
    if ($content_type =~ /^application\/json(;|$)/) {
        return decode_json($content);
    }
    else {
        # this=thing&that=other
        my @parts = split('&', $content);
        my %data = map { split('=', $_) } @parts;
        return \%data;
    }
}

1;

__END__

=head1 NAME

OAuth2::Simple - simple interface to OAuth2 providers

=head1 SYNOPSIS

Either use one of the stock providers:

    my $oauth2 = OAuth2::Simple->new(
        provider        => 'github',

        client_id       => 'xxxxx',
        client_secret   => 'yyyyy',
        redirect_uri    => 'zzzzz',
    );

Or roll-your-own:

    my $oauth2 = OAuth2::Simple->new(
        name => 'custom-provider',

        auth_uri        => 'aaaaa',
        token_uri       => 'bbbbb',

        client_id       => 'xxxxx',
        client_secret   => 'yyyyy',
        redirect_uri    => 'zzzzz',
    );

=head1 METHODS

=head2 auth_uri

Get the full query uri for the stage one auth request. The caller must provide
the state value, and is responsible for verifying it in the stage one redirect
callback.

=head2 request_token

Request the stage two access_token from the provider. Returns all the data from
the provider, including the access token itself.

=cut
