package HuNI::Backend::WebApp::User;
use 5.20.0;
use warnings;

use Exporter 'import';
our @EXPORT_OK = qw( may_be_logged_in must_be_logged_in );

use Function::Parameters qw( :strict );
use HuNI::Backend::WebApp::Util qw( resultset );
use MIME::Base64 qw( );

fun must_be_logged_in($app) {
    _check_for_user($app);
    return if $app->request->var('user');

    $app->send_error('Unauthorised', 403);
    $app->halt;
}

fun may_be_logged_in($app) {
    _check_for_user($app);
}

fun _key_huni_user($app)    { $app->setting('session_key')->{huni_user} }
fun _key_plack_user($app)   { $app->setting('session_key')->{plack_user} }
fun _key_plack_error($app)  { $app->setting('session_key')->{plack_error} }
fun _key_session_time($app) { $app->setting('session_key')->{session_time} }

fun _check_for_user($app) {

    if (my $plack_error = $app->session->read(_key_plack_error($app))) {
        # The user attempted to login but was unsuccessful for some reason
        _update_session($app, undef);   # clear out any session vars
        $app->send_error($plack_error->{error}, 403);
        $app->halt;
        return; # just to make it clear we don't proceed past here
    }

    # Regarding the ordering below:
    # The oauth2 user should trump the session user, and the api credentials
    # *should* probably do so as well, but the api key check is deliberately
    # slow, so we only check the api credentials if the session is missing
    # or expired.

    my $huni_user;
    $huni_user //= _check_for_oauth2_user($app);
    $huni_user //= _check_for_session_user($app);
    $huni_user //= _check_for_api_user($app);
    $huni_user //= _check_for_mock_user($app);

    _update_session($app, $huni_user);

    return;
}

fun _check_for_oauth2_user($app) {
    # Check if we've got an oauth2 user that's just signed in.
    my $oauth2_user = $app->session->read(_key_plack_user($app));
    return unless $oauth2_user;

    # Grab our local user details from the database, creating a new user
    # if this is their first time.
    return resultset('User')->find_or_create_from_oauth($oauth2_user);
}

fun _check_for_session_user($app) {
    my $huni_user = $app->session->read(_key_huni_user($app));
    return unless $huni_user;

    # Check whether this session has expired. If there is no time set,
    # automatically expire.
    my $session_last_used =
        $app->session->read(_key_session_time($app)) // 0;
    my $session_lifetime = $ENV{HUNI_BACKEND_SESSION_TIMEOUT}
                         // $app->setting('session_timeout')
                         // 60 * 60 * 24;
    if ($session_last_used + $session_lifetime < time()) {
        # This session has expired.
        return;
    }

    $huni_user->{new_user} = 0; # this user is no longer new
    return $huni_user;
}

fun _check_for_api_user($app) {
    # This code borrows from Plack::Middleware::Auth::Basic
    my $auth = $app->request->env->{HTTP_AUTHORIZATION} // '';
    return unless $auth =~ /^Basic (.*)$/i;

    my ($username, $key) = split /:/, (MIME::Base64::decode($1) || ":"), 2;

    # This will return undef if the credentials are wrong.
    return resultset('User')->find_from_api($username, $key);
}

fun _check_for_mock_user($app) {
    my $username = $ENV{HUNI_BACKEND_MOCK_USER};
    return unless $username;

    my $mock_user = {
        provider => 'mock',
        id      => $username,
        name    => $username,
        can_edit => 1,
    };

    # Add this user as if they came in via oauth2
    return resultset('User')->find_or_create_from_oauth($mock_user);
}

fun _update_session($app, $user) {
    # Clear out the whole session.
    for my $key (keys %{ $app->session->data }) {
        $app->session->delete($key);
    }

    # If we have a user, add them into the session.
    if ($user) {
        # Set the current time, not the projected expiry time, so we
        # can adjust the expiry time after the fact.
        $app->session->write(_key_huni_user($app),    $user);
        $app->session->write(_key_session_time($app), time());

        # Add the user as a Dancer2 'var'
        $app->request->var(user => $user);
    }
}

1;
