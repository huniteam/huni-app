package HuNI::Backend::WebApp::Handler::Link;
use Dancer2::Plugin;
use Function::Parameters    qw( :strict );
use Try::Tiny;

use strict;
use warnings;

use Log::Any qw( $log );
use HuNI::Backend::WebApp::Util qw( neo4j solr_select );
use REST::Neo4j::Util qw( with_counter inflate_timestamps );
with qw(
    HuNI::Backend::WebApp::Handler::Role::Abort
    HuNI::Backend::WebApp::Handler::Role::GetFields
);

method user_id() {
    return -1 unless exists $self->vars->{user};
    return $self->vars->{user}->{user_id};
}

# get '/link'
register fetch_links => method($app) {
    my @args = (
        parameters => {
            user_id => $self->user_id,
        },
    );

    my $result = neo4j->do(<<'...', @args);
MATCH ()-[link:Link { user_id: { user_id } }]->()
RETURN link
ORDER BY link.modified
...

    return [ map { inflate_timestamps($_->{row}->[0]) } @{ $result->{data} } ];
};

# get '/link/:id'
register fetch_link_details => method($app) {
    # Oh snap! It fails if I don't int-ify the link_id.
    my @args = (
        parameters => {
            user_id => $self->user_id,
            link_id => int($self->params->{id}),
        },
    );

    # TODO: merge this with the fetch_links query above
    my $result = neo4j->do(<<'...', @args);
MATCH (:Entity)-[link:Link { id: { link_id } }]->(:Entity)
RETURN link
...

    my @links = map { $_->{row}->[0] } @{ $result->{data} };
    $self->abort(404) unless @links;
    inflate_timestamps($links[0]);
    return $links[0];
};

method count_links($user_id, $linktype_id, $from_docid, $to_docid, $transaction_id = undef) {
    my %args = (
        parameters => {
            user_id     => $user_id,
            linktype_id => $linktype_id,
            from_docid  => $from_docid,
            to_docid    => $to_docid,
        }
    );
    $args{transaction_id} = $transaction_id if defined $transaction_id;

    my $result = neo4j->do(<<'...', %args);
MATCH (from:Entity)-[link:Link]->(to:Entity)
  WHERE (link.user_id      = {user_id}    and
         from.docid        = {from_docid} and
         to.docid          = {to_docid}   and
         link.linktype_id  = {linktype_id} )
     OR (link.user_id      = {user_id}    and
         from.docid        = {to_docid}   and
         to.docid          = {from_docid} and
         link.pairtype_id  = {linktype_id} )
RETURN count(*) AS linkCount
...

    return $result->{data}->[0]->{row}->[0];
}

# post '/link'
register create_link => method($app) {
    my %link = (
        user_id => $self->user_id,
        $self->get_fields(required => [qw( linktype_id from_docid to_docid )],
                          optional => [qw( synopsis )]),
    );
    $self->abort(400) if $link{from_docid} eq $link{to_docid};

    # Grab the docs from solr
    my $solr_docs = solr_select->find_docids(@link{qw( from_docid to_docid )});

    my $new_link;

    # Create the link, and then check how many we've got, and rollback if we
    # have more than one.
    my $create_link = fun($transaction_id, $link_id) {
        $link{id} = $link_id;

        delete $link{synopsis}
            if (!defined($link{synopsis}) || $link{synopsis} !~ /\S/);

        my @args = (
            parameters => {
                from => $solr_docs->{ $link{from_docid } },
                to   => $solr_docs->{ $link{to_docid } },
                link => \%link,
            },
            transaction_id => $transaction_id,
        );

        my $result = neo4j->do(<<'...', @args);
MERGE (from:Entity { docid: { from }.docid })
    ON CREATE SET from = { from }
    ON MATCH  SET from = { from }
MERGE (to:Entity { docid: { to }.docid })
    ON CREATE SET to = { to }
    ON MATCH  SET to = { to }
CREATE (from)-[link:Link { link }]->(to)
WITH from, to, link
MATCH (type:Linktype { id:link.linktype_id })
WITH link, type
SET link.linktype = type.name,
    link.pairtype = type.pair,
    link.pairtype_id = type.pair_id,
    link.created = timestamp(),
    link.modified = timestamp()
RETURN link
...

        my $count = $self->count_links(
            @link{qw( user_id linktype_id from_docid to_docid )},
            $transaction_id);
        if ($count != 1) {
            die "Link already exists ($count), rolling back";
        }

        $new_link = $result->{data}->[0]->{row}->[0];
        return $link_id + 1; # consume the counter value
    };
    eval { with_counter(neo4j, 'Link', $create_link) };

    $self->abort(400) unless $new_link;

    inflate_timestamps($new_link);
    return $new_link;
};

# post '/link/batch'
register add_link_batch => method($app) {
    my $links = $self->app->request->data;

    # Find all the solr documents mentioned in these links.
    my %docid;
    for my $link (@$links) {
        $docid{$link->{from_docid}}++;
        $docid{$link->{to_docid}}++;
    }
    my $solr_docs = solr_select->find_docids(keys %docid);

    # Create the input data for neo4j
    my @data;
    for my $link (@$links) {
        if (exists $link->{synopsis} && $link->{synopsis} eq '') {
            delete $link->{synopsis};
        }
        push(@data, {
            link => $link,
            from => $solr_docs->{ $link->{from_docid} },
            to   => $solr_docs->{ $link->{to_docid}   },
        });
    }

    my $inserted_links = 0;

    $log->info("Adding " . (scalar @$links) . " links");

    my $insert_batch = fun($transaction_id, $link_id) {
        my @args = (
            parameters => {
                links => \@data,
                user_id => $self->user_id,
                next_link => $link_id,
            },
            transaction_id => $transaction_id,
        );

        my $result = neo4j->do(<<'...', @args);
CREATE (counter { value: { next_link } })
WITH counter
UNWIND { links } as linkdata
    MERGE (from:Entity { docid: linkdata.from.docid })
        ON CREATE SET from = linkdata.from
        ON MATCH  SET from = linkdata.from
    MERGE (to:Entity { docid: linkdata.to.docid })
        ON CREATE SET to = linkdata.to
        ON MATCH  SET to = linkdata.to
    MERGE (from)-[link:Link {
            from_docid:     linkdata.from.docid,
            to_docid:       linkdata.to.docid,
            linktype_id:    linkdata.link.linktype_id,
            user_id:        { user_id }
        }]->(to)
        ON CREATE SET
            link.created     = timestamp(),
            link.modified    = timestamp(),
            link.pairtype_id = linkdata.link.pairtype_id,
            link.linktype    = linkdata.link.link_type,
            link.pairtype    = linkdata.link.pair_type,
            link.synopsis    = linkdata.link.synopsis,
            link.id          = counter.value,
            counter.value    = counter.value + 1
        ON MATCH SET
            link.synopsis    = coalesce(link.synopsis, linkdata.link.synopsis),
            link.modified    = timestamp()
WITH counter, counter.value as ret
DELETE counter
RETURN ret
...

        my $new_linkid = $result->{data}->[0]->{row}->[0];
        $inserted_links = $new_linkid - $link_id;

        return $new_linkid;
    };

    eval { with_counter(neo4j, 'Link', $insert_batch) };

    return {
        added => $inserted_links,
        duplicate => @$links - $inserted_links,
    }
};

# put '/link/:id'
register update_link => method($app) {
    my $param = $self->app->request->data;
    my @args = (
        parameters => {
            user_id => int($self->vars->{user}->{user_id}),
            link_id => int($self->params->{id}),
            linktype_id =>
                defined($param->{linktype_id}) && int($param->{linktype_id}),
            synopsis => $self->app->request->data->{synopsis},
        },
    );

    my $result = neo4j->do(<<'...', @args);
MATCH (:Entity)-[link:Link { id: { link_id }, user_id: { user_id } }]->(:Entity)
WITH link
SET link.linktype_id = coalesce({ linktype_id }, link.linktype_id),
    link.synopsis = coalesce({ synopsis }, link.synopsis)
WITH link
MATCH (type:Linktype { id:link.linktype_id })
WITH link, type
SET link.linktype = type.name,
    link.pairtype = type.pair,
    link.pairtype_id = type.pair_id,
    link.updated = timestamp()
RETURN link
...

    my $link = $result->{data}->[0]->{row}->[0];
    $self->abort(400) unless $link;
    return $link;
};

# delete '/link/:id'
register delete_link => method($app) {
    my @args = (
        parameters => {
            user_id => int($self->vars->{user}->{user_id}),
            link_id => int($self->params->{id}),
        },
    );
    my $result = neo4j->do(<<'...', @args);
MATCH (:Entity)-[link:Link { id: { link_id }, user_id: { user_id } }]->(:Entity)
DELETE link
RETURN link
...

    my $count = @{ $result->{data} };
    $self->abort(404) unless $count;
    $self->status(200);
    return undef;
};

register_plugin;

1;
