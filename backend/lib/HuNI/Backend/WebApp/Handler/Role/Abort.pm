package HuNI::Backend::WebApp::Handler::Role::Abort;

use 5.16.0;
use warnings;
use Moo::Role;
use Function::Parameters qw( :strict );

method abort($code = 404, $message = "$code") {
    $self->send_error($message, $code);
    $self->halt;
}

1;
