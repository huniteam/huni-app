package HuNI::Backend::WebApp::Handler::CMS;
use Dancer2::Plugin;
use Encode;
use Function::Parameters qw( :strict );
use Try::Tiny;

use HuNI::Backend::WebApp::Util qw( resultset schema );
with qw(
    HuNI::Backend::WebApp::Handler::Role::Abort
    HuNI::Backend::WebApp::Handler::Role::GetFields
);

sub rs {
    return resultset('Content');
}

# get '/cms'
register cms_list => method($app) {
    my $rs = rs->search({ }, {
        columns => [qw( id )],
        distinct => 1,
        order_by => 'id' })->get_column('id');
    return [ $rs->all ];
};

# get '/cms/:id/:version?'
register cms_get => method($app) {
    my $id      = $self->params->{id};
    my $version = $self->params->{version};

    my $row = defined $version ? rs->find({ id => $id, version => $version })
                               : rs->find_latest($id);

    my $html = $row ? $row->content : "CMS item <strong>$id</strong> not found";
    return $self->send_file(\$html, content_type => 'text/html');
};

# get '/cms/:id/history'
register cms_history => method($app) {
    my $id = $self->params->{id};

    return rs->document_history($id)->for_json;
};

# put '/cms/:id'
register cms_put => method($app) {
    $self->abort(403) unless $self->vars->{user}->{can_edit};
    my %fields = $self->get_fields(required => [qw( content )]);
    my $id = $self->params->{id};

    my $version = rs->insert_item(
        id      => $id,
        user_id => $self->vars->{user}->{user_id},
        content => Encode::encode('UTF-8', $fields{content}),
    );

    return { id => $id, version => $version };
};

register_plugin;

1;
