package HuNI::Backend::WebApp::Handler::Record;
use Dancer2::Plugin;
use Function::Parameters qw( :strict );

use strict;
use warnings;

use HuNI::Backend::WebApp::Util qw( lookup_users neo4j resultset );
use REST::Neo4j::Util           qw( normalise_links );
with qw(
    HuNI::Backend::WebApp::Handler::Role::Abort
    HuNI::Backend::WebApp::Handler::Role::GetFields
);

# get '/record/collection?d=docid&d=docid'
register fetch_collections_for_records => method($app) {
    my $query = $self->app->request->params('query');
    return { } unless exists $query->{d};

    my %user_query;
    if (exists $self->vars->{user}) {
        $user_query{'collection_users.user_id'}
            = $self->vars->{user}->{user_id};
    }

    my $rs = resultset('CollectionRecord')->search(
        {
            'me.docid' => $query->{d},
            -or => [
                'collection.is_public' => 1,
                %user_query,
            ],
        },
        {
            prefetch => [qw( collection )],
            join     => { collection => [qw( collection_users )] },
            order_by => [qw( collection.name    )],
        },
    );

    # Need to use $rs->all here - if we use $rs->next we get this error:
    #
    #   DBIx::Class::ResultSet::_construct_results(): Unable to properly
    #   collapse has_many results in iterator mode due to order criteria -
    #   performed an eager cursor slurp underneath. Consider using ->all()
    #   instead.
    #
    my %by_docid;
    for my $row ($rs->all) {
        push(@{ $by_docid{$row->docid} }, {
            id => $row->collection_id,
            name => $row->collection->name,
            is_public => $row->collection->is_public,
        });
    }

    return \%by_docid;
};

# get '/record/link?d=docid&d=docid'
register fetch_links_for_records => method($app) {
    my $query = $self->app->request->params('query');
    return { } unless exists $query->{d};

    my $parameters = { docids => $query->{d} };
    my $result = neo4j->do(<<'...', parameters => $parameters);
MATCH (d)-[r]-() WHERE d.docid in { docids }
RETURN d.docid as docid, count(r) as linkCount
...

    my %counts = map { $_->{row}->[0] => $_->{row}->[1] } @{ $result->{data} };

    return \%counts;
};

# get '/record/:id/collection'
register fetch_record_collections => method($app) {
    my $docid = $self->params->{id};
    my $inner = resultset('CollectionRecord')->search({ docid => $docid });
    my $rs = resultset('Collection')
                ->viewable_by($self->vars->{user}->{user_id})
                ->with_extra
                ->search({ 'me.id' => {
                    -in => $inner->get_column('collection_id')->as_query,
                  }});

    return $rs->for_json;
};

# Returns an array of
# {
#   // Link
#   "id":639,
#   "from_docid":"Bonza***production***6",
#   "to_docid":"CAARP***film***7982",
#   "created_utc":"2016-09-07T17:04:26.249 Z",
#   "modified_utc":"2016-09-07T17:04:26.249 Z",
#   "user_id":94,
#   "synopsis":"",
#   "linktype_id":186,
#
#   // Linktype
#   "linktype":"Same As",
#   "pairtype_id":186,
#   "pairtype":"Same As",
# }
#
# Need to add this in manually:
# {
#   // Postgres
#   "is_mine":0,
#   "username":"Huni Other Account Two",
# },

#TODO: return { link, linktype, todoc } from neo
# get '/record/:id/link'
register fetch_record_links => method($app) {
    my $docid = $self->params->{id};
    my $parameters = {
        docid => $docid,
    };

    my $result = neo4j->do(<<'...', parameters => $parameters);
MATCH (:Entity {docid:{ docid }})-[link]-()
RETURN link
...

    my @links = map { $_->{row}->[0] } @{ $result->{data} };
    normalise_links(\@links, $docid);

    my $user_id = $self->vars->{user} ? $self->vars->{user}->{user_id} : -1;
    lookup_users(\@links, $user_id);

    return \@links;
};

register_plugin;

1;
