use utf8;
package HuNI::Backend::Schema::Result::CollectionExtra;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

HuNI::Backend::Schema::Result::CollectionExtra

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<HuNI::Backend::Schema::Result>

=cut

use base 'HuNI::Backend::Schema::Result';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::InflateColumn::JSON>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "InflateColumn::JSON");
__PACKAGE__->table_class("DBIx::Class::ResultSource::View");

=head1 TABLE: C<huni.collection_extra>

=cut

__PACKAGE__->table("huni.collection_extra");
__PACKAGE__->result_source_instance->view_definition(" SELECT c.id AS collection_id,\n    COALESCE(cs.record_count, (0)::bigint) AS record_count,\n    COALESCE(co.owners, '[]'::json) AS owners,\n    COALESCE(ce.editors, '[]'::json) AS editors,\n    COALESCE(cv.viewers, '[]'::json) AS viewers\n   FROM ((((huni.collection c\n     LEFT JOIN ( SELECT collection_record.collection_id,\n            count(*) AS record_count\n           FROM huni.collection_record\n          GROUP BY collection_record.collection_id) cs ON ((c.id = cs.collection_id)))\n     LEFT JOIN ( SELECT cu.collection_id,\n            json_agg(json_build_object('id', cu.user_id, 'name', u.name) ORDER BY u.name) AS owners\n           FROM (huni.collection_user cu\n             JOIN huni.\"user\" u ON (((cu.user_id = u.id) AND (cu.role_id = 'owner'::text))))\n          GROUP BY cu.collection_id) co ON ((c.id = co.collection_id)))\n     LEFT JOIN ( SELECT cu.collection_id,\n            json_agg(json_build_object('id', cu.user_id, 'name', u.name) ORDER BY u.name) AS editors\n           FROM (huni.collection_user cu\n             JOIN huni.\"user\" u ON (((cu.user_id = u.id) AND (cu.role_id = 'read-write'::text))))\n          GROUP BY cu.collection_id) ce ON ((c.id = ce.collection_id)))\n     LEFT JOIN ( SELECT cu.collection_id,\n            json_agg(json_build_object('id', cu.user_id, 'name', u.name) ORDER BY u.name) AS viewers\n           FROM (huni.collection_user cu\n             JOIN huni.\"user\" u ON (((cu.user_id = u.id) AND (cu.role_id = 'read-only'::text))))\n          GROUP BY cu.collection_id) cv ON ((c.id = cv.collection_id)))");

=head1 ACCESSORS

=head2 collection_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 record_count

  data_type: 'bigint'
  is_nullable: 0

=head2 owners

  data_type: 'json'
  is_nullable: 0

=head2 editors

  data_type: 'json'
  is_nullable: 0

=head2 viewers

  data_type: 'json'
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "collection_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "record_count",
  { data_type => "bigint", is_nullable => 0 },
  "owners",
  { data_type => "json", is_nullable => 0 },
  "editors",
  { data_type => "json", is_nullable => 0 },
  "viewers",
  { data_type => "json", is_nullable => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-12-12 00:10:04
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:Gwk6Vz3+3y/AHuYjTYg0Dw

# Note there's custom_column_info defined for this view in update-dbic-schema

__PACKAGE__->has_one(
    collection => 'HuNI::Backend::Schema::Result::Collection',
    { 'foreign.id' => 'self.collection_id' },
);

# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
