use utf8;
package HuNI::Backend::Schema::Result::Collection;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

HuNI::Backend::Schema::Result::Collection

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<HuNI::Backend::Schema::Result>

=cut

use base 'HuNI::Backend::Schema::Result';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::InflateColumn::JSON>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "InflateColumn::JSON");

=head1 TABLE: C<huni.collection>

=cut

__PACKAGE__->table("huni.collection");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'huni.collection_id_seq'

=head2 name

  data_type: 'text'
  is_nullable: 0

=head2 synopsis

  data_type: 'text'
  is_nullable: 1

=head2 is_public

  data_type: 'boolean'
  default_value: false
  is_nullable: 1

=head2 created_utc

  data_type: 'timestamp'
  default_value: timezone('utc'::text, now())
  is_nullable: 0

=head2 modified_utc

  data_type: 'timestamp'
  default_value: timezone('utc'::text, now())
  is_nullable: 0

=head2 in_solr

  data_type: 'boolean'
  default_value: false
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "huni.collection_id_seq",
  },
  "name",
  { data_type => "text", is_nullable => 0 },
  "synopsis",
  { data_type => "text", is_nullable => 1 },
  "is_public",
  { data_type => "boolean", default_value => \"false", is_nullable => 1 },
  "created_utc",
  {
    data_type     => "timestamp",
    default_value => \"timezone('utc'::text, now())",
    is_nullable   => 0,
  },
  "modified_utc",
  {
    data_type     => "timestamp",
    default_value => \"timezone('utc'::text, now())",
    is_nullable   => 0,
  },
  "in_solr",
  { data_type => "boolean", default_value => \"false", is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 collection_records

Type: has_many

Related object: L<HuNI::Backend::Schema::Result::CollectionRecord>

=cut

__PACKAGE__->has_many(
  "collection_records",
  "HuNI::Backend::Schema::Result::CollectionRecord",
  { "foreign.collection_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 collection_users

Type: has_many

Related object: L<HuNI::Backend::Schema::Result::CollectionUser>

=cut

__PACKAGE__->has_many(
  "collection_users",
  "HuNI::Backend::Schema::Result::CollectionUser",
  { "foreign.collection_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-12-05 03:48:50
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:pP6AKdOI7DkwzHBTWuxq+w

# The many_to_many relationships don't get created automatically.
__PACKAGE__->many_to_many(users => 'collection_users', 'user');

use Role::Tiny::With;
with 'HuNI::Backend::Schema::Role::Result::Timestamps';

__PACKAGE__->has_one(
    extra => 'HuNI::Backend::Schema::Result::CollectionExtra',
    { 'foreign.collection_id' => 'self.id' },
);

use Carp qw( croak );
use Function::Parameters qw( :strict );

my @extra_json_columns = qw( record_count owners editors viewers );
method for_json() {
    my $json = $self->SUPER::for_json;

    if ($self->has_related('collection_records')) {
        $json->{records} = $self->collection_records->for_json;
    }

    if ($self->has_related('extra')) {
        for my $column (@extra_json_columns) {
            $json->{$column} = $self->extra->$column;
        }
    }

    $json->{docid} = 'HuNI***Collection***' . $json->{id};

    return $json;
}

# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
