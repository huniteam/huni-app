use utf8;
package HuNI::Backend::Schema::Result::AafJti;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

HuNI::Backend::Schema::Result::AafJti

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<HuNI::Backend::Schema::Result>

=cut

use base 'HuNI::Backend::Schema::Result';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::InflateColumn::JSON>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "InflateColumn::JSON");

=head1 TABLE: C<huni.aaf_jti>

=cut

__PACKAGE__->table("huni.aaf_jti");

=head1 ACCESSORS

=head2 jti

  data_type: 'text'
  is_nullable: 0

=head2 expiry

  data_type: 'integer'
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "jti",
  { data_type => "text", is_nullable => 0 },
  "expiry",
  { data_type => "integer", is_nullable => 0 },
);

=head1 UNIQUE CONSTRAINTS

=head2 C<aaf_jti_jti_key>

=over 4

=item * L</jti>

=back

=cut

__PACKAGE__->add_unique_constraint("aaf_jti_jti_key", ["jti"]);


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-12-05 03:48:50
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:kekFAZEfuy61X3rfvxWWbg


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
