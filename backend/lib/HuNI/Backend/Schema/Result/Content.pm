use utf8;
package HuNI::Backend::Schema::Result::Content;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

HuNI::Backend::Schema::Result::Content

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<HuNI::Backend::Schema::Result>

=cut

use base 'HuNI::Backend::Schema::Result';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::InflateColumn::JSON>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "InflateColumn::JSON");

=head1 TABLE: C<huni.content>

=cut

__PACKAGE__->table("huni.content");

=head1 ACCESSORS

=head2 id

  data_type: 'text'
  is_nullable: 0

=head2 version

  data_type: 'integer'
  is_nullable: 0

=head2 user_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 content

  data_type: 'bytea'
  is_nullable: 0

=head2 created_utc

  data_type: 'timestamp'
  default_value: timezone('utc'::text, now())
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "text", is_nullable => 0 },
  "version",
  { data_type => "integer", is_nullable => 0 },
  "user_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "content",
  { data_type => "bytea", is_nullable => 0 },
  "created_utc",
  {
    data_type     => "timestamp",
    default_value => \"timezone('utc'::text, now())",
    is_nullable   => 0,
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=item * L</version>

=back

=cut

__PACKAGE__->set_primary_key("id", "version");

=head1 RELATIONS

=head2 user

Type: belongs_to

Related object: L<HuNI::Backend::Schema::Result::User>

=cut

__PACKAGE__->belongs_to(
  "user",
  "HuNI::Backend::Schema::Result::User",
  { id => "user_id" },
  { is_deferrable => 1, on_delete => "RESTRICT", on_update => "CASCADE" },
);


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-12-05 03:48:50
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:gSBdk1M1x0QJl874gkyn4g


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
