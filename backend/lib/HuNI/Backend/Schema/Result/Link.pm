use utf8;
package HuNI::Backend::Schema::Result::Link;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

HuNI::Backend::Schema::Result::Link

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<HuNI::Backend::Schema::Result>

=cut

use base 'HuNI::Backend::Schema::Result';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::InflateColumn::JSON>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "InflateColumn::JSON");

=head1 TABLE: C<huni.link>

=cut

__PACKAGE__->table("huni.link");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'huni.link_id_seq'

=head2 user_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 synopsis

  data_type: 'text'
  is_nullable: 1

=head2 from_docid

  data_type: 'text'
  is_nullable: 0

=head2 to_docid

  data_type: 'text'
  is_nullable: 0

=head2 linktype_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 created_utc

  data_type: 'timestamp'
  default_value: timezone('utc'::text, now())
  is_nullable: 0

=head2 modified_utc

  data_type: 'timestamp'
  default_value: timezone('utc'::text, now())
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "huni.link_id_seq",
  },
  "user_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "synopsis",
  { data_type => "text", is_nullable => 1 },
  "from_docid",
  { data_type => "text", is_nullable => 0 },
  "to_docid",
  { data_type => "text", is_nullable => 0 },
  "linktype_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "created_utc",
  {
    data_type     => "timestamp",
    default_value => \"timezone('utc'::text, now())",
    is_nullable   => 0,
  },
  "modified_utc",
  {
    data_type     => "timestamp",
    default_value => \"timezone('utc'::text, now())",
    is_nullable   => 0,
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<forwards_unique>

=over 4

=item * L</user_id>

=item * L</from_docid>

=item * L</to_docid>

=item * L</linktype_id>

=back

=cut

__PACKAGE__->add_unique_constraint(
  "forwards_unique",
  ["user_id", "from_docid", "to_docid", "linktype_id"],
);

=head1 RELATIONS

=head2 linktype

Type: belongs_to

Related object: L<HuNI::Backend::Schema::Result::Linktype>

=cut

__PACKAGE__->belongs_to(
  "linktype",
  "HuNI::Backend::Schema::Result::Linktype",
  { id => "linktype_id" },
  { is_deferrable => 0, on_delete => "CASCADE", on_update => "CASCADE" },
);

=head2 user

Type: belongs_to

Related object: L<HuNI::Backend::Schema::Result::User>

=cut

__PACKAGE__->belongs_to(
  "user",
  "HuNI::Backend::Schema::Result::User",
  { id => "user_id" },
  { is_deferrable => 1, on_delete => "CASCADE", on_update => "CASCADE" },
);


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-12-05 03:48:50
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:CICbvbfgeWO1MB+gdGS5HA

use Role::Tiny::With;
with 'HuNI::Backend::Schema::Role::Result::Timestamps';

# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
