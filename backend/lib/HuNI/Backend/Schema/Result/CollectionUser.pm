use utf8;
package HuNI::Backend::Schema::Result::CollectionUser;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

HuNI::Backend::Schema::Result::CollectionUser

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<HuNI::Backend::Schema::Result>

=cut

use base 'HuNI::Backend::Schema::Result';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::InflateColumn::JSON>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "InflateColumn::JSON");

=head1 TABLE: C<huni.collection_user>

=cut

__PACKAGE__->table("huni.collection_user");

=head1 ACCESSORS

=head2 collection_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 user_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 role_id

  data_type: 'text'
  is_foreign_key: 1
  is_nullable: 0

=head2 inviting_user_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "collection_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "user_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "role_id",
  { data_type => "text", is_foreign_key => 1, is_nullable => 0 },
  "inviting_user_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</collection_id>

=item * L</user_id>

=item * L</role_id>

=back

=cut

__PACKAGE__->set_primary_key("collection_id", "user_id", "role_id");

=head1 UNIQUE CONSTRAINTS

=head2 C<collection_user_collection_id_user_id_key>

=over 4

=item * L</collection_id>

=item * L</user_id>

=back

=cut

__PACKAGE__->add_unique_constraint(
  "collection_user_collection_id_user_id_key",
  ["collection_id", "user_id"],
);

=head1 RELATIONS

=head2 collection

Type: belongs_to

Related object: L<HuNI::Backend::Schema::Result::Collection>

=cut

__PACKAGE__->belongs_to(
  "collection",
  "HuNI::Backend::Schema::Result::Collection",
  { id => "collection_id" },
  { is_deferrable => 0, on_delete => "CASCADE", on_update => "CASCADE" },
);

=head2 inviting_user

Type: belongs_to

Related object: L<HuNI::Backend::Schema::Result::User>

=cut

__PACKAGE__->belongs_to(
  "inviting_user",
  "HuNI::Backend::Schema::Result::User",
  { id => "inviting_user_id" },
  { is_deferrable => 1, on_delete => "CASCADE", on_update => "CASCADE" },
);

=head2 role

Type: belongs_to

Related object: L<HuNI::Backend::Schema::Result::CollectionRole>

=cut

__PACKAGE__->belongs_to(
  "role",
  "HuNI::Backend::Schema::Result::CollectionRole",
  { id => "role_id" },
  { is_deferrable => 1, on_delete => "CASCADE", on_update => "CASCADE" },
);

=head2 user

Type: belongs_to

Related object: L<HuNI::Backend::Schema::Result::User>

=cut

__PACKAGE__->belongs_to(
  "user",
  "HuNI::Backend::Schema::Result::User",
  { id => "user_id" },
  { is_deferrable => 1, on_delete => "CASCADE", on_update => "CASCADE" },
);


# Created by DBIx::Class::Schema::Loader v0.07047 @ 2017-12-05 03:48:50
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:jgveo6MWxFJ5Yzf6YLZ12w

use Function::Parameters qw( :strict );

method for_json() {
    my $json = $self->SUPER::for_json;

    if ($self->has_related('user')) {
        $json->{user} = {
            name => $self->user->name,
            id   => $self->user->id,
        };
    }

    if ($self->has_related('inviting_user')) {
        $json->{inviting_user} = {
            name => $self->inviting_user->name,
            id   => $self->inviting_user->id,
        };
    }

    return $json;
}

# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
