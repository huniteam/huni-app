package HuNI::Backend::Schema::Role::Result::Timestamps;

use 5.16.0;
use warnings;

use Role::Tiny;
use Function::Parameters qw( :strict );

method refresh_timestamps() { $self->discard_changes }

1;

__END__

=head1 NAME

HuNI::Backend::Schema::Role::Result::Timestamps

=head1 SYNOPSIS

In your Result class:

    use Role::Tiny::With;
    with 'HuNI::Backend::Schema::Role::Result::Timestamps';

=head1 DESCRIPTION

Timestamp related methods for result classes.

=head1 METHODS

=head2 refresh_timestamps

Just an alias for discard_changes. Reloads the row from the database so that
we get the refreshed timestamp.

=cut
