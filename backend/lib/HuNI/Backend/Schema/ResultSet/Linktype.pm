package HuNI::Backend::Schema::ResultSet::Linktype;

use 5.16.0;
use warnings;
use parent 'HuNI::Backend::Schema::ResultSet';
use Function::Parameters qw( :strict );

method find_or_create_linktype(:$from, :$to, :$name, :$pair, :$user_id) {
    eval {
        # Try creating the linktype first. If we have one already it will fail.
        $self->result_source->storage->dbh_do(fun ($storage, $dbh) {
            my @row = $dbh->selectrow_array(
                'SELECT huni.create_linktype(?, ?, ?, ?, ?);',
                undef,
                $from, $to, $name, $pair, $user_id,
            );
        });
    };

    return $self->find(
        {
            name        => $name,
            'pair.name' => $pair,
            'from.name' => $from,
            'to.name'   => $to,
        },
        {
            join => [qw( from to pair )],
        });
}

method for_categories($from, $to) {
    my $me = $self->current_source_alias;
    return $self->search(
        {
            'from.name' => $from,
            'to.name'   => $to,
        },
        {
            join        => [qw( from to )],
            order_by    => [ "$me.name" ],
        }
    );
}

1;

__END__

=head1 NAME

HuNI::Backend::Schema::ResultSet::User

=head1 METHODS

=head2 find_or_create_from_oauth

Wrapper around find_or_create which automatically converts the user details
provided by the OAuth2 plack middleware.

=cut
