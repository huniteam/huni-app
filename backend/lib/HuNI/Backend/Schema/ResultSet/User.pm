package HuNI::Backend::Schema::ResultSet::User;

use 5.16.0;
use warnings;
use parent 'HuNI::Backend::Schema::ResultSet';
use Function::Parameters qw( :strict );

method find_or_create_from_oauth($oauth) {
    my $auth_userid = _trim($oauth->{id});
    my $auth_domain = _trim($oauth->{provider});

    my $query = {
        auth_userid => $auth_userid,
        auth_domain => $auth_domain,
    };

    if (my $row = $self->find($query)) {
        $row->update({ auth_extra => $oauth->{extra} });
        return _huni_user($row, 0);
    }

    my $name  = _trim($oauth->{name});
    my $email = _trim($oauth->{email});
    if (! defined $name) {
        if (defined $email) {
            # use the email username
            $name = $email;
            $name =~ s/@.*$//;
        }
        else {
            $name = 'Worker Bee';
        }
    }
    $query->{name} = $name;
    $query->{email} = $email;
    $query->{auth_extra} = $oauth->{extra};

    if ($auth_domain eq 'fixture') {
        $query->{can_edit} = 1;
    }

    return _huni_user($self->create($query), 1);
}

method find_from_api($api_username, $api_key) {
    my $query = {
        api_username => $api_username,
    };

    # Try to find a user with this username, and check their key.
    if (my $row = $self->find($query)) {
        if ($row->check_api_key($api_key)) {
            return _huni_user($row, 0);
        }
    }

    # User not found, or key doesn't match. Ta ta.
    return;
}

fun _huni_user($row, $is_new) {
    return {
        display_name => $row->name,
        user_id      => $row->id,
        new_user     => $is_new,
        can_edit     => $row->can_edit,
    };
}

fun _trim($s) {
    return unless defined $s;
    my $ret = ($s =~ s/^\s*|\s*$//gr);
    return unless length $ret;
    return $ret;
}

1;

__END__

=head1 NAME

HuNI::Backend::Schema::ResultSet::User

=head1 METHODS

=head2 find_or_create_from_oauth

Wrapper around find_or_create which automatically converts the user details
provided by the OAuth2 plack middleware.

=cut
