package Plack::App::IndexFile;
use parent 'Plack::App::File';

sub locate_file
{
    my ($self, $env) = @_;
    my $path         = $env->{PATH_INFO} || '';

    return $self->SUPER::locate_file($env) unless $path && $path =~ m{^/$};
    $env->{PATH_INFO} .= 'index.html';
    return $self->SUPER::locate_file($env);
}

1;

__END__

=head1 NAME

Plack::App::IndexFile - Plack::App::File with index.html support

=head1 DESCRIPTION

Plack::App::File does not automatically serve index.html.

This module adds that ability.

=cut
