package Plack::App::Auth::OAuth2;
use parent qw( Plack::Component );

use strict;
use warnings;

use Plack::Request;
use Plack::Response;
use Plack::Session;
use Plack::Util::Accessor qw(
    providers

    ua
    handler
    oauth2
);

use Data::GUID          qw();
use HTTP::Tiny          qw();
use JSON                qw( encode_json );
use OAuth2::Simple      qw();
use OAuth2::UserDetails qw( get_details );
use Try::Tiny;
use URI::Escape         qw( uri_unescape );
use YAML                qw( LoadFile );;

my $login_sk = 'oauth2-login';
my $user_sk  = 'oauth2-user';
my $error_sk = 'oauth2-error';
my $default_redir = '/';

sub prepare_app {
    my ($self) = @_;

    for my $param (qw( providers )) {
        die "Must specify $param" unless defined $self->$param;
    }

    if (! ref $self->providers) {
        $self->providers(LoadFile($self->providers) // {});
    }

    $self->ua(HTTP::Tiny->new) unless $self->ua;
    $self->handler({});
    $self->oauth2({});

    for my $provider (keys %{ $self->providers }) {
        # Register the route handlers for this provider.
        $self->handler->{"/$provider"} = sub {
            $self->do_login_request(@_, $provider);
        };

        $self->handler->{"/$provider/reply"} = sub {
            $self->do_login_response(@_, $provider);
        };
    }

    $self->handler->{'/'} = sub { $self->get_provider_info };
}

sub make_oauth2 {
    my ($self, $req, $provider) = @_;

    return $self->oauth2->{$provider} //= OAuth2::Simple->new(
        provider        => $provider,
        client_id       => $self->providers->{$provider}->{client_id},
        client_secret   => $self->providers->{$provider}->{client_secret},
        ua              => $self->ua,
        redirect_uri    => $req->base . "/$provider/reply",
    );
}

sub call {
    my ($self, $env) = @_;

    my $req = Plack::Request->new($env);
    return $self->app->($env) unless $req->method eq 'GET';

    if (my $handler = $self->handler->{$req->path}) {
        my $session = Plack::Session->new($env);
        return $handler->($req, $session);
    }
    else {
        return Plack::Response->new(404)->finalize;
    }
}

sub do_login_request {
    my ($self, $req, $session, $provider) = @_;

    my $redir = uri_unescape($req->param('r') // $default_redir);
    my $state = Data::GUID->new->as_string;

    $session->set($login_sk => { state => $state, redir => $redir });
    $session->remove($user_sk);
    $session->remove($error_sk);

    my $oauth2 = $self->make_oauth2($req, $provider);
    my $uri = $oauth2->auth_uri($state);
    return $self->redirect($uri);
}

sub do_login_response {
    my ($self, $req, $session, $provider) = @_;

    my $oauth2_login = $session->get($login_sk);

    $session->remove($login_sk);

    try {
        my $params = $req->parameters;
        if (exists $params->{error_description}) {
            die $params->{error_description}, "\n";
        }

        if (!exists $params->{state} || !exists $params->{code}
            || !$oauth2_login) {
            die "Unexpected error\n";
        }

        my $state = $params->{state} // '';
        my $code  = $params->{code};

        if ($state ne $oauth2_login->{state}) {
            die "Login timed out\n"; # state mismatch - CSRF attempt?
        }

        my $oauth2 = $self->make_oauth2($req, $provider);
        my $token = $oauth2->request_token($code); # may die
        $session->set($user_sk => get_details(
            ua            => $self->ua,
            access_token  => $token->{access_token},
            provider      => $oauth2->provider,
            provider_data => $token, # send the raw data through as well
        ));
    }
    catch {
        chomp; # $_ has a newline to avoid the 'at $file $line' business
        $session->set($error_sk => { error => $_ });
    };

    my $redir_uri = URI->new($oauth2_login->{redir} // $default_redir);

    # Strip off the front part of the redirect, to avoid any unwanted redirects.
    my $redir = $redir_uri->path_query;
    $redir .= '#' . $redir_uri->fragment if defined $redir_uri->fragment;

    return $self->redirect($redir);
}

sub redirect {
    my ($self, $uri) = @_;

    my $response = Plack::Response->new;
    $response->redirect($uri);
    return $response->finalize;
}

sub get_provider_info {
    my ($self) = @_;

    my $info = OAuth2::Simple->provider_info;
    my $providers = [
        sort { $a->{name} cmp $b->{name} }
        map { +{ id => $_, name => $info->{$_}->{name} } }
            keys %{ $self->providers }
    ];
    # XXX: This is a bit of a hack. We really need to either:
    # 1. hoist this method up to a higher level and let it query OAuth2/AAF, or
    # 2. support multiple login types
    unshift(@$providers, { id => 'aaf', name => 'AAF' });
    my $response = Plack::Response->new(200,
        { 'Content-Type' => 'application/json' },
        encode_json($providers),
    );
    return $response->finalize;
}

1;

__END__

=head1 NAME

Plack::App::Auth::OAuth2

=cut
