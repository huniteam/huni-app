package Plack::App::Auth::AAF;

# https://rapid.aaf.edu.au/developers

use 5.20.0;
use warnings;

use parent qw( Plack::Component );

my (@config, @params);
BEGIN {
    @config = qw( aud iss request_uri secret );
    @params = qw( config validate_jti );
};

use Function::Parameters    qw( :strict );
use JSON::WebToken          qw( encode_jwt decode_jwt );
use Plack::Request          qw( );
use Plack::Response         qw( );
use Plack::Session          qw( );
use Plack::Util::Accessor   ( @config, @params );
use Try::Tiny;
use URI::Escape             qw( uri_unescape );
use YAML                    qw( LoadFile );

my $login_sk = 'oauth2-login';
my $user_sk  = 'oauth2-user';
my $error_sk = 'oauth2-error';
my $default_redir = '/';

method prepare_app() {
    for my $param (@params) {
        die "Must specify $param" unless defined $self->$param;
    }

    if (! ref $self->config) {
        $self->config(LoadFile($self->config));
    }

    for my $param (@config) {
        $self->$param($self->$param // $self->config->{$param})
            // die "Must specify $param in config";
    }
}

method call($env) {
    my $response = try {

        my $req = Plack::Request->new($env);
        my $session = Plack::Session->new($env);

        if (($req->method eq 'GET') && ($req->path eq '/')) {
            return $self->do_login_request($req, $session);
        }
        elsif (($req->method eq 'POST') && ($req->path eq '/reply')) {
            return $self->do_login_response($req, $session);
        }
        else {
            return undef;
        }
    }
    catch {
        warn $_;
        return undef;
    };

    $response //= Plack::Response->new(404);
    return $response->finalize;
}

method do_login_request($req, $session) {
    my $redir = uri_unescape($req->param('r') // $default_redir);

    $session->set($login_sk => { redir => $redir });
    $session->remove($user_sk);
    $session->remove($error_sk);

    return $self->redirect($self->request_uri);
}

method do_login_response($req, $session) {

    my $aaf_login = $session->get($login_sk);
    $session->remove($login_sk);

    try {
        my $jwt = $req->body_parameters->{assertion};
        my $claim = decode_jwt($jwt, $self->secret);

        $self->verify($claim);
        $session->set($user_sk => $self->get_user($claim));
    }
    catch {
        chomp; # Exceptions have \n to avoid the line number
        $session->set($error_sk => { error => $_ });
    };

    my $redir_uri = URI->new($aaf_login->{redir} // $default_redir);

    # Strip off the front part of the redirect, to avoid any unwanted redirects.
    my $redir = $redir_uri->path_query;
    $redir .= '#' . $redir_uri->fragment if defined $redir_uri->fragment;

    return $self->redirect($redir);
}

method verify($claim) {
    for my $field (qw( aud iss )) {
        die "Field $field mismatch\n"
            unless $claim->{$field} eq $self->$field;
    }

    my $now = time;
    die "Too early\n" unless $claim->{nbf} < $now;
    die "Expired\n"   unless $now < $claim->{exp};

    try {
        $self->validate_jti->($claim->{jti}, $claim->{exp});
    }
    catch {
        die "Duplicate JTI\n";
    };
}

method get_user($claim) {
    my $user = $claim->{'https://aaf.edu.au/attributes'};

    # See: https://validator.aaf.edu.au/snapshots/latest
    #
    # These fields don't match what we're actually getting back.
    #
    # What we get is:
    # {
    #   "cn": ...
    #   "displayname": ...
    #   "edupersonorcid": ...
    #   "edupersonprincipalname": ...
    #   "edupersonscopedaffiliation": ...
    #   "edupersontargetedid": ...
    #   "givenname": ...
    #   "mail": ...
    #   "surname": ...
    #  }
    my %extra;
    for my $key (qw( eduPersonAffiliation o eduPersonOrcid )) {
        $extra{$key} = $user->{$key};
    }
    return {
        provider => 'AAF',
        id       => $claim->{sub},
        name     => $user->{displayname} // $user->{cn},
        email    => $user->{mail},
        extra    => $user, #XXX temporary \%extra,
    };
}

method redirect($uri) {
    my $response = Plack::Response->new;
    $response->redirect($uri);
    return $response;
}

1;
