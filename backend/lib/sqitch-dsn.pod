=head1 Name

sqitch-dsn - Print dsn for a target.

=head1 Synopsis

Print out the dsn for the given target.

  sqitch [<sqitch options>] dsn [<dsn options>] [<database>]

=head1 Description

Prints out the dsn matching the specified database.

The C<< <database> >> parameter specifies the database to which to connect, and
may also be specified as the C<--target> option. It can be either a URI or the
name of a target in the configuration.

=head1 Options

=over

=item C<-t>

=item C<--target>

The target database to which to connect. This option can be either a URI or
the name of a target in the configuration.

=back
