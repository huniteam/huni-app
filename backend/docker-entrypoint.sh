#!/bin/bash
set -e

secrets=(
    neo4j_password
    neo4j_guest_password
    postgres_password
)

for secret_name in ${secrets[@]}; do
    env_name=$(echo ${secret_name} | awk '{ print toupper($0) }')

    if [ -f "${!env_name}" ]; then
        export $env_name="$(cat ${!env_name})"
    fi
done

export HUNI_SQITCH_TARGET="postgresql://${POSTGRES_USER}:${POSTGRES_PASSWORD}@${PGHOST}/${POSTGRES_DB}"
export HUNI_BACKEND_DSN="dbi:Pg:dbname=${POSTGRES_DB};user=${POSTGRES_USER};password=${POSTGRES_PASSWORD};host=${PGHOST};"
export SD_NEO4J_URI="http://${NEO4J_USER}:${NEO4J_PASSWORD}@${NEO4J_HOST}:${NEO4J_PORT}/"

exec "$@"
