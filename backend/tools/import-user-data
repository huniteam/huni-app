#!/usr/bin/env perl

use 5.16.0;
use warnings;

use HuNI::Backend::Schema;
use Function::Parameters qw( :strict );
use JSON();
use HTTP::Tiny();

{
    package Options;
    use Moo;
    use MooX::Options;

    option dsn => (
        is => 'ro',
        format => 's',
        doc => 'Database DSN',
        required => 1,
    );

    option email => (
        is => 'ro',
        format => 's',
        doc => 'User email',
        required => 1,
    );

    option dry_run => (
        is => 'ro',
        doc => 'Dry run',
        default => 0,
    );
}

my $opt = Options->new_with_options;

exit main(%$opt) ? 0 : 1;

#------------------------------------------------------------------------------

fun main(:$dsn, :$email, :$dry_run) {
    my $schema = HuNI::Backend::Schema->connect($dsn);
    my $user = find_user($schema, $email);
    my $data = JSON->new->decode(do { undef $/; <STDIN> });

    $schema->txn_do(sub {
        import_collection($user, $_)    for @{ $data->{collections} };
        import_link($schema, $user, $_) for @{ $data->{links} };
        if ($dry_run) {
            die "DRY RUN ONLY!\n";
        }
    });

    return 1;
}

fun docid_exists($docid) {
    say "?? CHECKING docid: $docid";
    my $uri = 'https://huni.net.au/solr/select?rows=0&wt=json&q=docid:' . $docid;
    my $res = HTTP::Tiny->new->get($uri);
    die $res->{reason} . " returned from $uri" unless $res->{success};

    my $data = JSON->new->decode($res->{content});
    return $data->{response}->{numFound} != 0;
}

fun find_user($schema, $email) {
    my $users = $schema->resultset('User')->search({ email => $email });
    if ($users->count == 0) {
        die "User $email not found";
    }
    elsif ($users->count > 1) {
        die "User $email found ${ \$users->count } times";
    }
    return $users->single;
}

fun extract_fields($hash, @fields) {
    return { map { $_ => $hash->{$_} } @fields };
}

fun import_collection($user, $collection) {
    my $name = $collection->{name};
    if ($user->collections->find({ name => $name })) {
        say "-- SKIPPING collection: $name";
        return;
    }

    say "++ CREATING collection: $name";
    my $row = $user->add_to_collections(
        extract_fields($collection, qw( name is_public synopsis ))
    );
    for my $record (@{ $collection->{records} }) {
        my $docid = $record->{docid};
        if (docid_exists($docid)) {
            say "++ CREATING record: $docid";
            $row->add_to_collection_records(
                extract_fields($record, qw( docid ))
            );
        }
        else {
            say "!! MISSING  docid $docid";
            say "-- SKIPPING record: $docid";
        }
    }
}

fun import_link($schema, $user, $link) {
    # find_or_create_linktype does-or-dies which doesn't play well with
    # transactions
    my $linktype = $link->{linktype};
    my $linkdesc = sprintf('%s -> %s/%s -> %s',
        $linktype->{from}->{name},
        $linktype->{name},
        $linktype->{pairname},
        $linktype->{to}->{name},
    );
    my $row = $schema->resultset('Linktype')->find({
        name        => $linktype->{name},
        'pair.name' => $linktype->{pairname},
        'from.name' => $linktype->{from}->{name},
        'to.name'   => $linktype->{to}->{name},
    }, { join => [qw( from to pair )] });
    if ($row) {
        say "-- SKIPPING linktype: $linkdesc";
    }
    else {
        say "++ CREATING linktype: $linkdesc";
        $row = $schema->resultset('Linktype')->find_or_create_linktype(
            name => $link->{linktype}->{name},
            pair => $link->{linktype}->{pairname},
            from => $link->{linktype}->{from}->{name},
            to   => $link->{linktype}->{to}->{name},
            user_id => $user->id,
        );
    }

    $linkdesc = sprintf('%s -> %s/%s -> %s',
        $link->{from_docid},
        $linktype->{name},
        $linktype->{pairname},
        $link->{to_docid},
    );
    for my $id (qw( from_docid to_docid )) {
        my $docid = $link->{$id};
        if (!docid_exists($docid)) {
            say "!! MISSING  docid: $docid";
            say "-- SKIPPING linktype: $linkdesc";
            return;
        }
    }
    my $fields = extract_fields($link, qw( from_docid to_docid ));
    $fields->{linktype_id} = $row->id;
    if ($user->links->find($fields)) {
        say "-- SKIPPING link: $linkdesc";
    }
    else {
        say "++ CREATING link: $linkdesc";
        $user->add_to_links($fields);
    }

}

1;
