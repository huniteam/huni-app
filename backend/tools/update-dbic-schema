#!/usr/bin/env perl

use 5.16.0;
use warnings;

use DBIx::Class::Schema::Loader qw( make_schema_at );
use Function::Parameters        qw( :strict );

update_schema($ENV{HUNI_BACKEND_DSN});
exit 0;

fun update_schema($dsn) {
    my ($dsn) = @_;

    my $options = {
        # our tables are in the huni schema
        db_schema       => 'huni',

        # prefix table names with huni namespace eg. huni.user
        qualify_objects => 1,

        # set this as the base class for all resultset classes
        default_resultset_class => '+HuNI::Backend::Schema::ResultSet',

        # set this as the base class for all result classes
        result_base_class => 'HuNI::Backend::Schema::Result',

        custom_column_info => \&custom_column_info,

        dump_directory  => './lib',
        components      => [qw(
            InflateColumn::DateTime
            InflateColumn::JSON
        )],
    };
    make_schema_at(
        'HuNI::Backend::Schema',
        $options,
        [ $dsn ],
    );
}

fun custom_column_info($table, $column_name, $column_info) {
    if ($table->name eq 'collection_extra') {
        my $info = {
            is_nullable => 0,
        };
        if ($column_name eq 'collection_id') {
            $info->{is_foreign_key} = 1;
        }
        return $info;
    }

    return;
}

__END__

=head1 NAME

update-dbic-schema

=head1 DESCRIPTION

Creates or updates the DBIx::Class schema from the latest sqitch change.

Performs these steps:

    1. Create a fresh database with Test::PostgreSQL
    2. Deploy database schema with sqitch deploy
    3. Build DBIx::Class schema files with dbicdump

Any configuration options required to generate the desired DBIx::Class code
should go in here.

=cut




