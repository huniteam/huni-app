#!/usr/bin/env perl

use 5.16.0;
use warnings;

use Function::Parameters qw( :strict );

use HuNI::Backend::Schema;
use Path::Tiny;

{
    package Options;
    use Moo;
    use MooX::Options;

    option dsn => (
        is => 'ro',
        format => 's',
        doc => 'Database DSN',
        required => 1,
    );

    option dir => (
        is => 'ro',
        format => 's',
        doc => 'Directory to load cms files from',
        required => 1,
    );
}

my $opt = Options->new_with_options;
exit(main($opt) ? 0 : 1);

fun main($opt) {
    my $schema = HuNI::Backend::Schema->connect($opt->dsn);
    my $rs = $schema->resultset('Content');

    say $opt->dir;

    my $it = path($opt->dir)->iterator;
    while (my $file = $it->()) {
        next unless $file->is_file;
        next unless $file->basename =~ /\.html$/;

        my $id = $file->basename;
        $id =~ s/\..*$//; # no extension

        # If we have this file already, leave it be.
        next if $rs->search({ id => $id })->count > 0;

        $rs->create({
            id      => $id,
            user_id => 1,
            version => 1,
            content => $file->slurp_raw,
        });
    }
    return 1;
}
