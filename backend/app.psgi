use 5.20.0;
use Log::Any::Adapter qw( FileHandle );
use Plack::Builder;
use Plack::App::Auth::AAF;
use Plack::App::Auth::OAuth2;
use Plack::App::Download::CSV;
use Plack::App::IndexFile;
use Plack::App::Proxy;
use HuNI::Backend::WebApp;
use HuNI::Backend::WebApp::Util qw( resultset );

# CONFIGURATION
#
# HUNI_BACKEND_ENABLE_CORS
# - define this to allow cross-origin requests - currently used for the
#   backend on dev01. Not intended for production or staging.
#
# HUNI_BACKEND_STATIC_ROOT
# - define this to serve the static files via the plack app. Not intended for
#   production or staging - these will be handled directly by nginx.
#
# HUNI_BACKEND_DELAY
# - define this to introduce a delay into the backend calls to simulate slow
#   connections. Delay is integer seconds.
#
# HUNI_BACKEND_MOCK_USER
# - define this to a string automatically log in a user with that name.
#   Not intended for staging or production.
#
# HUNI_BACKEND_OAUTH_CONF
# - location of oauth.yml config file
#   - defaults to oauth.yml
#
# HUNI_BACKEND_AAF_CONF
# - location of aaf.yml config file
#   - defaults to aaf.yml
#
# HUNI_BACKEND_SOLR_URI
# - location of remote solr endpoint - used for local development with a remote
#   solr node. https://huni.net.au/solr/select works
#
# HUNI_BACKEND_NO_SOLR_UPDATE
# - if set, don't update solr (useful for using production solr in testing)
#
# HUNI_BACKEND_SOLR_UPDATE
# - location of remote solr update endpoint - used to override the one in the
#   config. This is not an external facing endpoint.
#       eg. http://testing.huni.net.au:8080/solr/huni0001_master/update
#
# HUNI_BACKEND_DSN
# - override the database dsn


my %session_opts;

if (exists $ENV{HUNI_BACKEND_STATIC_ROOT}) {
    # In dev mode, give the session cookies a long life
    $session_opts{expires} = 60 * 60 * 24 * 30,
}

builder {
    enable 'Plack::Middleware::ReverseProxy';

    enable_if { exists $ENV{HUNI_BACKEND_ENABLE_CORS} } 'CrossOrigin',
        origins => '*', methods => '*', headers => '*';

    enable 'Session::Cookie',
        %session_opts,
        secret =>
            '5bBdCQ15sMNzLljPQy5zWvbZ2kvxsXhEA80kZ3MO1CMcVjwveAgyFS4Dnu24WnX';

    if (exists $ENV{HUNI_BACKEND_SOLR_URI}) {
        my $path = '/solr/select';
        my $remote_uri = $ENV{HUNI_BACKEND_SOLR_URI};
        mount $path =>
            Plack::App::Proxy->new(
                backend => 'LWP',
                remote => $remote_uri,
            )->to_app;
    }

    mount '/vlab/login/aaf' =>
        Plack::App::Auth::AAF->new(
            config => $ENV{HUNI_BACKEND_AAF_CONF} // 'aaf.yml',
            validate_jti => sub { resultset('AafJti')->validate(@_) },
        )->to_app;

    if (!exists $ENV{HUNI_BACKEND_OAUTH_CONF} ||
            $ENV{HUNI_BACKEND_OAUTH_CONF} ne '') {
        mount '/vlab/login' =>
        Plack::App::Auth::OAuth2->new(
            providers => $ENV{HUNI_BACKEND_OAUTH_CONF} // 'oauth.yml'
        )->to_app;
    }

    mount '/vlab/export' =>
        Plack::App::Download::CSV->new->to_app;

    if (exists $ENV{HUNI_BACKEND_STATIC_ROOT}) {
        mount '/' => Plack::App::IndexFile->new(
            root => $ENV{HUNI_BACKEND_STATIC_ROOT}
        )->to_app;
    }

    mount '/vlab' => builder {
        enable_if { exists $ENV{HUNI_BACKEND_DELAY} }
            'Plack::Middleware::Delay', delay => $ENV{HUNI_BACKEND_DELAY};

        enable 'Headers',
            set => ['Cache-Control' => 'no-store, no-cache, must-revalidate'];

        HuNI::Backend::WebApp->dance;
    };
}
